﻿using System.Collections.Generic;
using TaxModel.EntityDTO.Entities;
using TaxModel.EntityDTO.Enums;

namespace TaxModel.UnitTests.BusinessLogicLayer
{
    public class MockData
    {
        public List<ServiceEntity> ServiceEntities { get; set; }
        public List<BusinessPlanEntity> BusinessPlanEntities { get; set; }

        public ServiceEntity GetServiceEntity => ServiceEntities[0];
        public BusinessPlanEntity GetBusinessPlanEntity => BusinessPlanEntities[0];

        public MockData()
        {
            ServiceEntities = FillServiceEntities();
            BusinessPlanEntities = FillBusinessPlanEntities();
        }

        private List<ServiceEntity> FillServiceEntities()
        {
            return new List<ServiceEntity>
            {
                new ServiceEntity("name-01", "DisplayName01", "Desc01-statusVisAll", Status.VisibleToAll),
                new ServiceEntity("name-02", "DisplayName02", "Desc02-statusVisAdmin", Status.VisibleToAdmin),
                new ServiceEntity("name-03", "DisplayName03", "Desc03-statusInvis", Status.Invisible),
                new ServiceEntity("name-04", "DisplayName04", "Desc04-statusVisAll", Status.VisibleToAll),
                new ServiceEntity("name-05", "DisplayName05", "Desc05-statusVisAdmin", Status.VisibleToAdmin),
                new ServiceEntity("name-06", "DisplayName06", "Desc06-statusInvis", Status.Invisible)
            };
        }

        private List<BusinessPlanEntity> FillBusinessPlanEntities()
        {
            return new List<BusinessPlanEntity>
            {
                new BusinessPlanEntity("name-01", "DisplayName01", "Desc01-statusInvis", 1.50m),
                new BusinessPlanEntity("name-02", "DisplayName02", "Desc02-statusVisAdmin", 2.50m),
                new BusinessPlanEntity("name-03", "DisplayName03", "Desc03-statusVisAll", 3.50m),
                new BusinessPlanEntity("name-04", "DisplayName04", "Desc04-statusInvis", 4.50m),
                new BusinessPlanEntity("name-05", "DisplayName05", "Desc05-statusVisAdmin", 5.50m),
                new BusinessPlanEntity("name-06", "DisplayName06", "Desc06-statusVisAll", 6.50m)
            };
        }
    }
}
