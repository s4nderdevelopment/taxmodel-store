using System;
using System.Collections.Generic;
using System.Linq;
using TaxModel.EntityDTO.Entities;
using TaxModel.EntityDTO.Enums;
using TaxModel.Exceptions.Service;
using TaxModel.Interface.Repositories;

namespace TaxModel.UnitTests.BusinessLogicLayer.Repositories.Mock
{
    public class MockServiceRepository : IServiceRepository
    {
        private readonly ICollection<ServiceEntity> _serviceEntities;
        private readonly MockData _data;

        public MockServiceRepository()
        {
            _serviceEntities = new List<ServiceEntity>();
            _data = new MockData();
        }

        public ServiceEntity CreateService(ServiceEntity entity)
        {
            if (HasServiceWithName(entity.Name))
            {
                throw new ServiceDuplicateNameException("name = " + entity.Name);
            }
            _serviceEntities.Add(entity);
            return entity;
        }

        public ServiceEntity GetFirstServiceByName(string name)
        {
            foreach (ServiceEntity entity in _data.ServiceEntities)
            {
                if (entity.Name == name)
                {
                    return entity;
                }
            }
            throw new ServiceNotFoundException("name = " + name);
        }

        public ICollection<ServiceEntity> GetAllVisibleServices()
        {
            return _data.ServiceEntities.Where((ServiceEntity entity) => entity.Status == Status.VisibleToAll).ToList();
        }

        public ServiceEntity UpdateService(ServiceEntity updateEntity)
        {

            ServiceEntity entity = _data.ServiceEntities.FirstOrDefault(n => n.Name == updateEntity.Name);
            if (entity == null)
            {
                throw new ServiceNotFoundException("Could not retrieve service for updating.");
            }
            entity.Update(updateEntity);

            return entity;

        }

        public ICollection<ServiceEntity> GetAllVisibleToAdminServices()
        {
            ICollection<ServiceEntity> entityToReturn = _data.ServiceEntities.Where((ServiceEntity entity) => entity.Status == Status.VisibleToAdmin || entity.Status == Status.VisibleToAll).ToList();
            return entityToReturn;
        }


        public void Delete(ServiceEntity deleteEntity)
        {
            throw new NotImplementedException();
        }

        public bool HasServiceWithName(string name)
        {
            try
            {
                GetFirstServiceByName(name);
                return true;
            }
            catch (ServiceNotFoundException)
            {
                return false;
            }
        }

        public void Save()
        {

        }

        public void Save(bool acceptChangesOnSuccess)
        {

        }

        public void Dispose()
        {

        }
    }
}
