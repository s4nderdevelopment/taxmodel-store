﻿using System;
using System.Collections.Generic;
using System.Linq;
using TaxModel.EntityDTO.Entities;
using TaxModel.Interface.Repositories;

namespace TaxModel.UnitTests.BusinessLogicLayer.Repositories.Mock
{
    public class MockServiceBusinessPlanPaymentPlanRepository : IServiceBusinessPlanPaymentPlanRepository
    {
        private readonly ICollection<ServiceBusinessPlanPaymentPlanEntity> entities;
        public ICollection<ServiceBusinessPlanPaymentPlanEntity> GetByBusinessPlanName(Guid businessPlanId)
        {
            return entities.Where(entity => entity.BusinessPlan == businessPlanId).ToList();
        }

        public ICollection<ServiceBusinessPlanPaymentPlanEntity> GetByServiceName(Guid serviceId)
        {
            return entities.Where(entity => entity.Service == serviceId).ToList();
        }

        public void Save()
        {
        }

        public void Save(bool acceptChangesOnSuccess)
        {
        }
    }
}
