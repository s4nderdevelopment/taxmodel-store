﻿using System;
using System.Collections.Generic;
using TaxModel.EntityDTO.Entities;
using TaxModel.Exceptions.BusinessPlan;
using TaxModel.Interface.Repositories;

namespace TaxModel.UnitTests.BusinessLogicLayer.Repositories.Mock
{
    public class MockBusinessPlanRepository : IBusinessPlanRepository
    {
        private readonly ICollection<BusinessPlanEntity> businessPlanEntities;
        private readonly MockData _data;

        public MockBusinessPlanRepository()
        {
            businessPlanEntities = new List<BusinessPlanEntity>();
            _data = new MockData();
        }

        public BusinessPlanEntity CreateBusinessPlan(BusinessPlanEntity entity)
        {
            if (HasBusinessPlanWithName(entity.Name))
            {
                throw new BusinessPlanDuplicateNameException("name = " + entity.Name);
            }
            businessPlanEntities.Add(entity);
            return entity;
        }

        public BusinessPlanEntity GetBusinessPlanByName(string name)
        {
            foreach (BusinessPlanEntity entity in _data.BusinessPlanEntities)
            {
                if (entity.Name == name)
                {
                    return entity;
                }
            }
            throw new BusinessPlanNotFoundException("name = " + name);
        }

        public BusinessPlanEntity GetBusinessPlanById(Guid id)
        {
            foreach (BusinessPlanEntity entity in _data.BusinessPlanEntities)
            {
                if (entity.Id == id)
                {
                    return entity;
                }
            }
            throw new BusinessPlanNotFoundException("id = " + id);
        }

        private bool HasBusinessPlanWithName(string name)
        {
            try
            {
                GetBusinessPlanByName(name);
                return true;
            }
            catch (BusinessPlanNotFoundException)
            {
                return false;
            }
        }

        public void Save()
        {

        }

        public void Save(bool acceptChangesOnSuccess)
        {

        }
    }
}
