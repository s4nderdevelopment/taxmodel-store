using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TaxModel.EntityDTO.Entities;
using TaxModel.Exceptions.BusinessPlan;
using TaxModel.Interface.Converters;
using TaxModel.Interface.Repositories;
using TaxModel.Layers.BLL.Repositories;

namespace TaxModel.UnitTests.BusinessLogicLayer.Services
{
    [TestClass]
    public class BusinessPlanRepositoryTests
    {
        private IBusinessPlanRepository businessPlanRepository;
        private DbContext businessPlanContext;

        [TestInitialize]
        public void ArrangeService()
        {
            DbContextOptions<BusinessPlanRepository> options = new DbContextOptionsBuilder<BusinessPlanRepository>().UseInMemoryDatabase(databaseName: "TaxModelTestingDB").Options;

            BusinessPlanRepository productionBusinessPlanRepository = new BusinessPlanRepository(options);
            businessPlanRepository = productionBusinessPlanRepository;
            businessPlanContext = productionBusinessPlanRepository;
            businessPlanContext.Database.EnsureDeleted();
        }

        [TestMethod]
        public void TestCreateBusinessPlan_Succeeds()
        {
            //Arrange
            BusinessPlanEntity businessPlanCreateEntity = new BusinessPlanEntity("business-plan-random-guid", "Business Plan 1", "Description of: Business Plan 1.", 1532.0m, 0);

            //Act
            BusinessPlanEntity businessPlanEntity = businessPlanRepository.CreateBusinessPlan(businessPlanCreateEntity);

            //Assert
            Assert.AreEqual(businessPlanEntity.Name, businessPlanCreateEntity.Name);
            Assert.AreEqual(businessPlanEntity.DisplayName, businessPlanCreateEntity.DisplayName);
            Assert.AreEqual(businessPlanEntity.Description, businessPlanCreateEntity.Description);
            Assert.AreEqual(businessPlanEntity.Price, businessPlanCreateEntity.Price);
        }

        [TestMethod]
        public void TestCreateBusinessPlan_Fails_BusinessPlanDuplicateNameException()
        {
            //Arrange
            BusinessPlanEntity businessPlanCreateEntity = new BusinessPlanEntity("business-plan-random-guid", "Business Plan 1", "Description of: Business Plan 1.", 1533.0m, 0);
            businessPlanContext.Add(businessPlanCreateEntity);
            businessPlanContext.SaveChanges();
            BusinessPlanEntity businessPlanCreateEntityDuplicate = new BusinessPlanEntity("business-plan-random-guid", "Business Plan 1 with duplicate name", "Description of: Business Plan 1 with duplicate name.", 1532.0m, 0);

            //Act
            Action act = new Action(() =>
            {
                businessPlanRepository.CreateBusinessPlan(businessPlanCreateEntityDuplicate);
            });

            //Assert
            Assert.ThrowsException<BusinessPlanDuplicateNameException>(act);
        }

        [TestMethod]
        public void TestGetBusinessPlanByName_Succeeds()
        {
            //Arrange
            string businessPlanName = "business-plan-random-guid";
            BusinessPlanEntity businessPlanCreateEntity = new BusinessPlanEntity(businessPlanName, "Business Plan 1", "Description of: Business Plan 1.", 8734.0m, 0);
            businessPlanContext.Add(businessPlanCreateEntity);
            businessPlanContext.SaveChanges();

            //Act
            BusinessPlanEntity businessPlanEntity = businessPlanRepository.GetBusinessPlanByName(businessPlanName);

            //Assert
            Assert.AreEqual(businessPlanEntity.Name, businessPlanCreateEntity.Name);
            Assert.AreEqual(businessPlanEntity.DisplayName, businessPlanCreateEntity.DisplayName);
            Assert.AreEqual(businessPlanEntity.Description, businessPlanCreateEntity.Description);
            Assert.AreEqual(businessPlanEntity.Price, businessPlanCreateEntity.Price);
        }

        [TestMethod]
        public void TestGetBusinessPlanByName_Fails_BusinessPlanNotFoundException()
        {
            //Arrange
            string businessPlanName = "business-plan-random-guid";

            //Act
            Action act = new Action(() =>
            {
                businessPlanRepository.GetBusinessPlanByName(businessPlanName);
            });

            //Assert
            Assert.ThrowsException<BusinessPlanNotFoundException>(act);
        }
    }
}
