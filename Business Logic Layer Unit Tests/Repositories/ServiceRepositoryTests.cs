using Microsoft.EntityFrameworkCore;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using TaxModel.EntityDTO.Entities;
using TaxModel.EntityDTO.Enums;
using TaxModel.Exceptions.Service;
using TaxModel.Interface.Repositories;
using TaxModel.Layers.BLL.Services;

namespace TaxModel.UnitTests.BusinessLogicLayer.Services
{
    [TestClass]
    public class ServiceRepositoryTests
    {
        private IServiceRepository sRepository;
        private DbContext context;
        private string ServiceName;
        private MockData mockdata;


        [TestInitialize]
        public void ArrangeService()
        {
            DbContextOptions<ServiceRepository> options = new DbContextOptionsBuilder<ServiceRepository>().UseInMemoryDatabase(databaseName: "TaxModelTestingDB").Options;
            ServiceRepository ProdRepo = new ServiceRepository(options);
            context = ProdRepo;
            sRepository = ProdRepo;

            context.Database.EnsureDeleted();
            ServiceName = "Service-random-guid";
            mockdata = new MockData();
            context.AddRange(mockdata.ServiceEntities);
            context.SaveChanges();
        }


        [TestMethod]
        public void TestServiceCreation_Success()
        {
            //Arrange
            ServiceEntity test = new ServiceEntity(ServiceName, "Testname", "TestDescription", Status.VisibleToAll);


            //Act
            ServiceEntity createdServiceEntity = sRepository.CreateService(test);

            //Assert
            Assert.AreEqual(test.Name, createdServiceEntity.Name);
            Assert.AreEqual(test.DisplayName, createdServiceEntity.DisplayName);
            Assert.AreEqual(test.Description, createdServiceEntity.Description);
            Assert.AreEqual(test.Status, createdServiceEntity.Status);
        }

        public void TestServiceCreation_Fail()
        {
            //Arrange
            ServiceEntity test = new ServiceEntity(ServiceName, "Testname", "TestDescription", Status.VisibleToAll);
            ServiceEntity test2 = new ServiceEntity(ServiceName, "Testname", "TestDescription", Status.VisibleToAll);

            //Act
            Action action = new Action(() =>
            {
                ServiceEntity createdServiceEntity = sRepository.CreateService(test);
                ServiceEntity createdServiceEntity2 = sRepository.CreateService(test2);
            });

            Assert.ThrowsException<ServiceCreateException>(action);     //Checks if there was really an error by adding the same names
        }

        [TestMethod]
        public void TestGetFirstServiceByName_Success()
        {
            //Arrange
            ServiceEntity test1 = new ServiceEntity(ServiceName, "Testname", "TestDescription", Status.VisibleToAll);
            context.Add(test1);
            context.SaveChanges();

            //Act
            ServiceEntity createdServiceEntity1 = sRepository.GetFirstServiceByName(ServiceName);

            //Assert
            Assert.AreEqual(test1.Name, createdServiceEntity1.Name);
            Assert.AreEqual(test1.DisplayName, createdServiceEntity1.DisplayName);
            Assert.AreEqual(test1.Description, createdServiceEntity1.Description);
            Assert.AreEqual(test1.Status, createdServiceEntity1.Status);
        }

        [TestMethod]
        public void TestGetAllVisibleServices_Success()
        {
            //Arrange

            //Act
            List<ServiceEntity> GetActivatedServices = new List<ServiceEntity>(sRepository.GetAllVisibleServices());

            //Assert
            Assert.AreEqual(2, GetActivatedServices.Count);
            Assert.AreEqual(Status.VisibleToAll, GetActivatedServices[0].Status);
            Assert.AreEqual(Status.VisibleToAll, GetActivatedServices[1].Status);
        }
        [TestMethod]
        public void TestUpdateService_Success()
        {

            //Arrange
            ServiceEntity test1 = new ServiceEntity("name-03", "DisplayName03", "Desc03-statusInvis", Status.Invisible);
            //Act
            ServiceEntity testUpdate = sRepository.UpdateService(test1);
            //Assert

            Assert.AreEqual(testUpdate.Name, test1.Name);
            Assert.AreEqual(testUpdate.DisplayName, test1.DisplayName);
            Assert.AreEqual(testUpdate.Description, test1.Description);
            Assert.AreEqual(testUpdate.Status, Status.Invisible);
        }
        [TestMethod]
        public void TestUpdateService_ExceptionTest()
        {
            //Arrange
            ServiceEntity test1 = new ServiceEntity("FailedName", "Testname", "TestDescription", Status.VisibleToAll);
            //Act   
            Action action = new Action(() =>
            {
                ServiceEntity testUpdate = sRepository.UpdateService(test1);
            });
            //Assert
            Assert.ThrowsException<ServiceNotFoundException>(action);       // Error should be thrown when editing unfound name
        }



    }
}
