using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TaxModel.EntityDTO.DTOs.BusinessPlan;
using TaxModel.EntityDTO.Enums;
using TaxModel.Exceptions.BusinessPlan;
using TaxModel.Interface.Converters;
using TaxModel.Interface.Repositories;
using TaxModel.Interface.Services;
using TaxModel.Layers.BLL.Services;
using TaxModel.Startup.MappingProfiles;
using TaxModel.UnitTests.BusinessLogicLayer.Repositories.Mock;

namespace TaxModel.UnitTests.BusinessLogicLayer.Services
{
    [TestClass]
    public class BusinessPlanServiceTests
    {
        private IBusinessPlanService bpService;
        private IBusinessPlanRepository bpMockRepo;
        private IServiceRepository serviceMockRepository;
        private IServiceBusinessPlanPaymentPlanRepository serviceBpPaymentplanMockRepository;
        private IMapper mapper;

        [TestInitialize]
        public void Arrange()
        {
            //Arrange

            bpMockRepo = new MockBusinessPlanRepository();
            serviceBpPaymentplanMockRepository = new MockServiceBusinessPlanPaymentPlanRepository();
            serviceMockRepository = new MockServiceRepository();
            MapperConfiguration mapperConfig = new MapperConfiguration( cfg => cfg.AddProfile<MappingProfile>());
            mapper = mapperConfig.CreateMapper();

            bpService = new BusinessPlanService(bpMockRepo, serviceBpPaymentplanMockRepository, serviceMockRepository, mapper);
        }

        [TestMethod]
        public void TestCreateBusinessPlan1_Succeed()
        {
            //Arrange
            CreateBusinessPlanDTO createBP_DTO1 = new CreateBusinessPlanDTO("BusinessPlan 1", "This is business plan 1", 1234.00m);

            //Act
            BusinessPlanDTO bp_DTO1 = bpService.CreateBusinessPlan(createBP_DTO1);

            //Assert
            Assert.AreEqual(createBP_DTO1.DisplayName, bp_DTO1.DisplayName);
            Assert.AreEqual(createBP_DTO1.Description, bp_DTO1.Description);
            Assert.AreEqual(createBP_DTO1.Price, bp_DTO1.Price);
        }

        [TestMethod]
        public void TestCreateBusinessPlan2_Throws_BusinessPlanPriceTooHighException()
        {
            //Arrange
            CreateBusinessPlanDTO createBP_DTO2 = new CreateBusinessPlanDTO("BusinessPlan 2", "This is business plan 2, with extra description", 100000000.0m);

            //Act
            Action action = new Action(() =>
            {
                bpService.CreateBusinessPlan(createBP_DTO2);
            });

            //Assert
            Assert.ThrowsException<BusinessPlanPriceTooHighException>(action);
        }

        [TestMethod]
        public void TestGetBusinessPlanByName1_Throws_BusinessPlanNotFoundException()
        {
            //Arrange
            string nonExistantBusinessPlanName = "non-existant-business-plan-random-guid";

            //Act
            Action action = new Action(() =>
            {
                bpService.GetBusinessPlanByName(nonExistantBusinessPlanName);
            });

            //Assert
            Assert.ThrowsException<BusinessPlanNotFoundException>(action);
        }

        [TestMethod]
        public void TestGetBusinessPlanByName_Succeed()
        {
            //Arrange

            //Act
            BusinessPlanDTO dto = bpService.GetBusinessPlanByName("name-01");

            //Assert
            Assert.AreEqual(dto.Name, "name-01");
            Assert.AreEqual(dto.DisplayName, "DisplayName01");
            Assert.AreEqual(dto.Description, "Desc01-statusInvis");
            Assert.AreEqual(dto.Price, 1.50m);
        }

        [TestMethod]
        public void TestGetBusinessPlanByName_Fails_BusinessPlanNotFoundException()
        {
            //Arrange
            string nonExistantBusinessPlanName = "non-existant-business-plan-random-guid";

            //Act
            Action act = new Action(() =>
            {
                bpService.GetBusinessPlanByName(nonExistantBusinessPlanName);
            });

            //Assert
            Assert.ThrowsException<BusinessPlanNotFoundException>(act);
        }

        [TestMethod]
        public void Test_EditBusinessPlan_Succeeds()
        {
            //Arrange
            EditBusinessPlanDTO dto = new EditBusinessPlanDTO("name-03", "DisplayNameTest", "DescriptionTest", 1000, Status.VisibleToAll);

            //Act
            BusinessPlanDTO entity = bpService.EditBusinessPlan(dto);

            //Assert
            Assert.AreEqual("name-03", entity.Name);
            Assert.AreEqual("DisplayNameTest", entity.DisplayName);
        }

    }
}
