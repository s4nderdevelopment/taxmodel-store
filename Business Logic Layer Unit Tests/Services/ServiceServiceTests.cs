using AutoMapper;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using TaxModel.EntityDTO.DTOs.Service;
using TaxModel.EntityDTO.Entities;
using TaxModel.EntityDTO.Enums;
using TaxModel.Exceptions.Service;
using TaxModel.Interface.Converters;
using TaxModel.Interface.Repositories;
using TaxModel.Interface.Services;
using TaxModel.Layers.BLL.Services;
using TaxModel.Startup.MappingProfiles;
using TaxModel.UnitTests.BusinessLogicLayer.Repositories.Mock;

namespace TaxModel.UnitTests.BusinessLogicLayer.Services
{
    [TestClass]
    public class ServiceServiceTests
    {
        private IServiceService serviceService;
        private IServiceRepository mockServiceRepository;
        private IMapper mapper;

        [TestInitialize]
        public void Arrange()
        {
            MapperConfiguration mapperConfig = new MapperConfiguration(cfg => cfg.AddProfile<MappingProfile>());
            mapper = mapperConfig.CreateMapper();
            mockServiceRepository = new MockServiceRepository();
            serviceService = new ServiceService(mapper, mockServiceRepository);
        }

        [TestMethod]
        public void TestCreateService_Succeeds()
        {
            //Arrange
            CreateServiceDTO CreateS_DTO1 = new CreateServiceDTO
            {
                DisplayName = "service-1-displayname",
                Description = "This is Service 1 Description"
            };

            //Act
            ServiceDTO S_DTO1 = serviceService.CreateService(CreateS_DTO1);

            //Assert 
            Assert.AreEqual(CreateS_DTO1.DisplayName, S_DTO1.DisplayName);
            Assert.AreEqual(CreateS_DTO1.Description, S_DTO1.Description);
        }
        [TestMethod]
        public void Test_UpdateService_Succeeds()
        {
            EditServiceDTO dto = new EditServiceDTO("DisplayNameTest", "DescriptionTest", "name-03", Status.VisibleToAll);

            ServiceDTO entity = serviceService.Update(dto);

            Assert.AreEqual("name-03", entity.Name);
            Assert.AreEqual("DisplayNameTest", entity.DisplayName);
            Assert.AreEqual(Status.VisibleToAll, entity.Status);
        }

        [TestMethod]
        public void Test_UpdateService_ServiceNotFound()
        {
            EditServiceDTO dto = new EditServiceDTO("DisplayNameTest", "DescriptionTest", "testname000", Status.VisibleToAll);

            Assert.ThrowsException<ServiceNotFoundException>(
                new Action(() => serviceService.Update(dto)));
        }

        [TestMethod]
        public void TestGetAllVisibleServices_Succeeds()
        {
            ICollection<ServiceDTO> testlist = serviceService.GetAllVisibleServices();

            //Assert
            Assert.AreEqual(2, testlist.Count);
        }
        [TestMethod]
        public void TestGetAllVisibleToAdminServices_Succeeds()
        {
            ICollection<ServiceDTO> testlist = serviceService.GetAllVisibleToAdminServices();

            //Act
            int admincount = testlist.Count(n => n.Status == Status.VisibleToAdmin);
            int usercount = testlist.Count(n => n.Status == Status.VisibleToAll);
            int hiddencount = testlist.Count(n => n.Status == Status.Invisible);

            //Assert
            Assert.AreEqual(0, hiddencount);
            Assert.AreEqual(2, admincount);
            Assert.AreEqual(2, usercount);
            Assert.AreEqual(4, testlist.Count);
        }
        [TestMethod]
        public void Test_GetServiceByName_Succeeds()
        {
            string name = "name-04";

            ServiceEntity entity = mockServiceRepository.GetFirstServiceByName(name);

            //Assert
            Assert.AreEqual(name, entity.Name);
        }

        [TestMethod]
        public void Test_GetServiceByName_UnknownName()
        {
            string name = "service-random-guid-unknown";

            Assert.ThrowsException<ServiceNotFoundException>(
                new Action(() => serviceService.GetServiceByName(name)));
        }


    }
}
