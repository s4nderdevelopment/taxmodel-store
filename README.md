# TaxModel Store

For a school project at Fontys Hogescholen, me and 4 other students created a new store for Tax Model where clients can select a service with a business plan.

Although the project was not finished at the end of the semester, the steakholders from Tax Model were very satisfied with the result.

## Note

Because the original git repository (on the GitLab instance of my school) contains student emails, I decided to remove the history and just put the code online.

## Building / Running

1. Create a MySQL database called `taxmodel` and import the sql file `taxmodel.sql` from the current directory.
2. In `TaxModel Store/appsettings.json`, replace `username` to your MySQL username and `password` by your MySQL password. Depending on your environment, you might also have to update the server ip/hostname.
3. Run `dotnet build` to build the project.
4. Run `dotnet run --project TaxModel\ Store/TaxModel\ Store.csproj` to run the startup project.
