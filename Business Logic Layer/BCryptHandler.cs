﻿using TaxModel.Interface;
using BCr = BCrypt.Net.BCrypt;
using System;

namespace TaxModel.Layers.BLL
{
    public class BCryptHandler : IEncryptionHandler
    {
        /// <summary>
        /// Generates salt and uses it to hash the provided password. Returns salt in 'out' and password in return value.
        /// </summary>
        /// <param name="password">Password to hash</param>
        /// <param name="salt">Generated salt, returned via out.</param>
        /// <returns></returns>
        public string HashPassword(string password, out string salt)
        {
            salt = BCr.GenerateSalt();
            string hashedPass = BCr.HashPassword(password, salt);
            Console.WriteLine(hashedPass);
            return hashedPass;
        }

        public bool Verify(string inputPassword, string hashedPassword)
        {
            return BCr.Verify(inputPassword, hashedPassword);
        }
    }
}
