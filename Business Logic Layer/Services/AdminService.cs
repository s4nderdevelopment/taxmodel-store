﻿using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using TaxModel.EntityDTO.DTOs.Admin;
using Microsoft.EntityFrameworkCore;
using TaxModel.EntityDTO.DTOs.Admin;
using TaxModel.EntityDTO.Entities;
using TaxModel.EntityDTO.Helpers;
using TaxModel.Exceptions.Admin;
using TaxModel.Interface.Converters;
using TaxModel.Interface.Repositories;
using TaxModel.Interface;
using TaxModel.Interface.Converters;
using TaxModel.Interface.Repositories;
using TaxModel.Interface.Services;
using TaxModel.Startup.Models;

namespace TaxModel.Layers.BLL.Services
{
    public class AdminService : IAdminService
    {
        private readonly IAdminConverter _adminConverter;
        private readonly IAdminRepository _adminRepository;
        private readonly IEncryptionHandler _encryptionHandler;
        
        private readonly AppSettings _appSettings;

        public AdminService(IOptions<AppSettings> appSettings, IAdminConverter adminConverter, IAdminRepository adminRepo, IEncryptionHandler encryptionHandler)
        {
            _appSettings = appSettings.Value;
            _adminConverter = adminConverter;
            _adminRepository = adminRepo;
            _encryptionHandler = encryptionHandler;
        }

        public AuthenticateResponseAdmin Authenticate(AuthenticateRequest model)
        {
            AdminEntity admin = _adminRepository.GetByEmail(model.Email);

            // return null if user not found
            if (admin == null)
            {
                throw new AdminInvalidCredentialsException();
            }

            if (!BCrypt.Net.BCrypt.Verify(model.Password, admin.PasswordHash))
            {
                throw new AdminInvalidCredentialsException(); 
            }

            // authentication successful so generate jwt token
            var token = GenerateJwtToken(admin);

            return new AuthenticateResponseAdmin(admin, token);
        }

        public AdminDTO GetByEmail(string email)
        {
            AdminEntity entity = _adminRepository.GetByEmail(email);
            if(entity == null)
            {
                throw new NotImplementedException(""); //TODO not implemented yet
            }
            return _adminConverter.ConvertToAdminDTO(entity);
        }

        public ICollection<AdminDTO> GetAllAdmins() //New for authenticate
        {
            ICollection<AdminEntity> entities = _adminRepository.GetAllAdmins();
            return _adminConverter.ConvertToDTOs(entities);
        }

        public AdminDTO Create(CreateAdminDTO dto)
        {
            try
            {
                dto.Password = _encryptionHandler.HashPassword(dto.Password, out string salt);
                AdminEntity entity = _adminConverter.ConvertToAdminEntity(dto);
                entity.Salt = salt;
                AdminEntity newEntity = _adminRepository.Create(entity);
                _adminRepository.Save();
                AdminDTO retVal = _adminConverter.ConvertToAdminDTO(newEntity);

                return retVal;
            }
            catch (DbUpdateException)
            {
                return default;
            }
        }

        private string GenerateJwtToken(AdminEntity admin)
        {
            // generate token that is valid for 1 days
            //TODO: duration
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                    {
                        new Claim("Name", admin.Name),
                        new Claim("email", admin.Email),
                    }
                ),
                Expires = DateTime.UtcNow.AddDays(1),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

    }
}
