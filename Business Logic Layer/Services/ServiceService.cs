﻿using AutoMapper;
using System;
using System.Collections.Generic;
using TaxModel.EntityDTO.DTOs.Service;
using TaxModel.EntityDTO.Entities;
using TaxModel.Interface.Converters;
using TaxModel.Interface.Repositories;
using TaxModel.Interface.Services;

namespace TaxModel.Layers.BLL.Services
{
    public class ServiceService : IServiceService
    {
        private readonly IMapper _mapper;
        private readonly IServiceRepository _serviceRepository;

        public ServiceService(IMapper mapper, IServiceRepository serviceRepository)
        {
            _mapper = mapper;
            _serviceRepository = serviceRepository;
        }
        public ServiceDTO CreateService(CreateServiceDTO insertPresentationServiceDTO)
        {
            ServiceEntity entity = _serviceRepository.CreateService(_mapper.Map<ServiceEntity>(insertPresentationServiceDTO));
            ServiceDTO dto = _mapper.Map<ServiceDTO>(entity);
            _serviceRepository.Save();
            return dto;
        }

        public ServiceDTO Update(EditServiceDTO dto)
        {
            ServiceEntity updateEntity = _mapper.Map<ServiceEntity>(dto);
            ServiceEntity newEntity = _serviceRepository.UpdateService(updateEntity);
            _serviceRepository.Save();
            return _mapper.Map<ServiceDTO>(newEntity);
        }

        public bool Delete(string name)
        {
            try
            {
                ServiceEntity deleteEntity = _serviceRepository.GetFirstServiceByName(name);
                _serviceRepository.Delete(deleteEntity);
                _serviceRepository.Save();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public ICollection<ServiceDTO> GetAllVisibleServices()
        {
            ICollection<ServiceEntity> entities = _serviceRepository.GetAllVisibleServices();
            return _mapper.Map<ICollection<ServiceDTO>>(entities);
        }

        public ICollection<ServiceDTO> GetAllVisibleToAdminServices()
        {
            ICollection<ServiceEntity> entities = _serviceRepository.GetAllVisibleToAdminServices();
            return _mapper.Map<ICollection<ServiceDTO>>(entities);
        }

        public ServiceDTO GetServiceByName(string name)
        {
            ServiceEntity entity = _serviceRepository.GetFirstServiceByName(name);
            return _mapper.Map<ServiceDTO>(entity);
        }
    }
}
