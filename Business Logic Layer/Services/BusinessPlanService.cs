﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using TaxModel.EntityDTO.DTOs.BusinessPlan;
using TaxModel.EntityDTO.Entities;
using TaxModel.Exceptions.BusinessPlan;
using TaxModel.Interface.Converters;
using TaxModel.Interface.Repositories;
using TaxModel.Interface.Services;

namespace TaxModel.Layers.BLL.Services
{
    public class BusinessPlanService : IBusinessPlanService
    {
        private readonly IBusinessPlanRepository _businessPlanRepository;
        private readonly IServiceBusinessPlanPaymentPlanRepository _serviceBusinessPlanPaymentPlanRepository;
        private readonly IServiceRepository _serviceRepository;
        private readonly IMapper _mapper;

        public BusinessPlanService(IBusinessPlanRepository businessPlanRepository, IServiceBusinessPlanPaymentPlanRepository serviceBusinessPlanPaymentPlanRepository, IServiceRepository serviceRepository, IMapper mapper)
        {
            _businessPlanRepository = businessPlanRepository;
            _serviceBusinessPlanPaymentPlanRepository = serviceBusinessPlanPaymentPlanRepository;
            _serviceRepository = serviceRepository;
            _mapper = mapper;
        }

        public BusinessPlanDTO CreateBusinessPlan(CreateBusinessPlanDTO createDTO)
        {
            if (createDTO.Price >= 1E7m)
            {
                throw new BusinessPlanPriceTooHighException("" + createDTO.Price);
            }

            // Removes all not allowed characters. Allowed characters are lowercase a-z, 0-9 and a dash (-).
            string name = createDTO.DisplayName.Trim().ToLower();
            Regex rgx = new Regex("[^a-zA-Z0-9 ]");
            name = rgx.Replace(name, "").Trim();
            name = name.Replace(" ", "-") + "-" + Guid.NewGuid().ToString();

            // TODO Test if strings are not too long for inserting in database. Otherwise throw exception.

            try
            {
                BusinessPlanEntity businessPlan = _businessPlanRepository.CreateBusinessPlan(_mapper.Map<BusinessPlanEntity>(createDTO));
                BusinessPlanDTO dto = new BusinessPlanDTO(businessPlan.Name, businessPlan.DisplayName, businessPlan.Description, businessPlan.Price, businessPlan.Status);

                _businessPlanRepository.Save();

                return dto;
            }
            catch (Exception)
            {
                throw new BusinessPlanDuplicateNameException(name);
            }
        }

        public BusinessPlanDTO GetBusinessPlanByName(string name)
        {
            BusinessPlanEntity entity = _businessPlanRepository.GetBusinessPlanByName(name);
            return new BusinessPlanDTO(entity.Name, entity.DisplayName, entity.Description, entity.Price, entity.Status);
        }

        public ICollection<BusinessPlanDTO> GetBusinessPlansByServiceName(string serviceName)
        {
            try
            {
                Guid serviceId = _serviceRepository.GetFirstServiceByName(serviceName).Id;
                ICollection<ServiceBusinessPlanPaymentPlanEntity> serviceBusinessPlanPaymentPlans = _serviceBusinessPlanPaymentPlanRepository.GetByServiceName(serviceId);

                ICollection<BusinessPlanDTO> businessPlanDTOs = new List<BusinessPlanDTO>();

                foreach (ServiceBusinessPlanPaymentPlanEntity serviceBusinessPlanPaymentPlan in serviceBusinessPlanPaymentPlans)
                {
                    BusinessPlanEntity businessPlan = _businessPlanRepository.GetBusinessPlanById(serviceBusinessPlanPaymentPlan.BusinessPlan);
                    businessPlanDTOs.Add(_mapper.Map<BusinessPlanDTO>(businessPlan));
                }

                return businessPlanDTOs;
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        public BusinessPlanDTO EditBusinessPlan(EditBusinessPlanDTO editDTO)
        {
            if (editDTO.Price >= 1E7m)
            {
                throw new BusinessPlanPriceTooHighException("" + editDTO.Price);
            }

            BusinessPlanEntity businessPlan = _businessPlanRepository.GetBusinessPlanByName(editDTO.Name);
            businessPlan.DisplayName = editDTO.DisplayName;
            businessPlan.Description = editDTO.Description;
            businessPlan.Price = editDTO.Price;
            businessPlan.Status = editDTO.Status;



            _businessPlanRepository.Save();

            return _mapper.Map<BusinessPlanDTO>(businessPlan);
        }
    }
}
