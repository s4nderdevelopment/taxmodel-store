using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using TaxModel.EntityDTO.DTOs.Client;
using TaxModel.EntityDTO.Entities;
using TaxModel.EntityDTO.Helpers;
using TaxModel.Exceptions.Client;
using TaxModel.Interface;
using TaxModel.Interface.Converters;
using TaxModel.Interface.Repositories;
using TaxModel.Interface.Services;
using TaxModel.Startup.Models;

namespace TaxModel.Layers.BLL.Services
{
    public class ClientService : IClientService
    {

        private readonly IMapper _mapper;
        private readonly IClientConverter _clientConverter;
        private readonly IClientRepository _clientRepository;
        private readonly IEncryptionHandler _encryptionHandler;

        private readonly AppSettings _appSettings;

        public ClientService(IOptions<AppSettings> appSettings, IMapper mapper, IClientRepository clientRepository, IClientConverter clientConverter, IEncryptionHandler encryptionHandler)
        {
            _appSettings = appSettings.Value;
            _mapper = mapper;
            _clientRepository = clientRepository;
            _encryptionHandler = encryptionHandler;
            _clientConverter = clientConverter;
        }

        public AuthenticateResponseClient Authenticate(AuthenticateRequest model)
        {
            ClientEntity client = _clientRepository.GetByEmail(model.Email);

            // return null if user not found
            if (client == null)
            {
                throw new ClientInvalidCredentialsException();
            };

            if (!BCrypt.Net.BCrypt.Verify(model.Password, client.PasswordHash))
            {
                throw new ClientInvalidCredentialsException();
            }

            // authentication successful so generate jwt token
            var token = GenerateJwtToken(client);

            return new AuthenticateResponseClient(client, token);
        }

        public ICollection<ClientDTO> GetAllClients() //New for authenticate
        {
            ICollection<ClientEntity> entities = _clientRepository.GetAllClients();
            return _clientConverter.ConvertToDTOs(entities);
        }

        public ClientDTO GetByEmail(string email)
        {
            ClientEntity entity = _clientRepository.GetByEmail(email);
            if(entity == null)
            {
                throw new NotImplementedException();//TODO: Throw correct error here...
            }
            return _clientConverter.ConvertToClientDTO(entity);
        }
        // helper methods

        private string GenerateJwtToken(ClientEntity client)
        {
            // generate token that is valid for 7 days
            // //TODO: duration
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(new[]
                {
                    new Claim("name", client.Name),
                    new Claim("email", client.Email),
                    new Claim("phoneNumer", client.PhoneNumber),
                    new Claim("companyName", client.CompanyName),
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }

        public ClientDTO Create(CreateClientDTO dto)
        {
            try
            {
                dto.Password = _encryptionHandler.HashPassword(dto.Password, out string salt);
                ClientEntity entity = _mapper.Map<ClientEntity>(dto);
                entity.Salt = salt;
                ClientEntity newEntity = _clientRepository.Create(entity);
                _clientRepository.Save();
                ClientDTO retVal = _mapper.Map<ClientDTO>(newEntity);

                return retVal;
            }
            catch (DbUpdateException)
            {
                return default;
            }
        }
    }
}
