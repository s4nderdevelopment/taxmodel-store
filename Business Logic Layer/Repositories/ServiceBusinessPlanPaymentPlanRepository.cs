﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using TaxModel.EntityDTO.Entities;
using TaxModel.Interface.Repositories;

namespace TaxModel.Layers.BLL.Repositories
{
    public class ServiceBusinessPlanPaymentPlanRepository : DbContext, IServiceBusinessPlanPaymentPlanRepository
    {
        protected DbSet<ServiceBusinessPlanPaymentPlanEntity> BusinessPlanServices { get; set; }

        public ServiceBusinessPlanPaymentPlanRepository(DbContextOptions<ServiceBusinessPlanPaymentPlanRepository> options) : base(options) {}

        public ICollection<ServiceBusinessPlanPaymentPlanEntity> GetByBusinessPlanName(Guid businessPlanGuid)
        {
            ICollection<ServiceBusinessPlanPaymentPlanEntity> entity = BusinessPlanServices.Where((ServiceBusinessPlanPaymentPlanEntity entity) => entity.BusinessPlan == businessPlanGuid).ToList();
            return entity;
        }

        public ICollection<ServiceBusinessPlanPaymentPlanEntity> GetByServiceName(Guid serviceId)
        {
            ICollection<ServiceBusinessPlanPaymentPlanEntity> entity = BusinessPlanServices.Where((ServiceBusinessPlanPaymentPlanEntity entity) => entity.Service == serviceId).ToList();
            return entity;
        }

        public void Save()
        {
            SaveChanges(true);
        }
        public void Save(bool acceptChangesOnSuccess)
        {
            SaveChanges(acceptChangesOnSuccess);
        }
    }
}
