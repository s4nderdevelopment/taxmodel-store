﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Collections.Generic;
using System.Linq;
using TaxModel.EntityDTO.Entities;
using TaxModel.EntityDTO.Enums;
using TaxModel.Exceptions.Service;
using TaxModel.Interface.Repositories;

namespace TaxModel.Layers.BLL.Services
{
    public class ServiceRepository : DbContext, IServiceRepository
    {
        protected DbSet<ServiceEntity> Services { get; set; }

        public ServiceRepository(DbContextOptions<ServiceRepository> options) : base(options)
        {
        }

        public ServiceEntity CreateService(ServiceEntity createEntity)
        {
            EntityEntry entry = Add(createEntity);
            if (entry.State != EntityState.Added)
            {
                throw new ServiceCreateException("Service already exists!");
            }
            ServiceEntity entity = entry.Entity as ServiceEntity;
            return entity;
        }

        public ServiceEntity UpdateService(ServiceEntity updateEntity)
        {
            ServiceEntity entity = Services.FirstOrDefault(n => n.Name == updateEntity.Name);
            if (entity == null)
            {
                throw new ServiceNotFoundException("Could not retrieve service for updating.");
            }
            entity.Update(updateEntity);
            return entity;
        }

        public ServiceEntity GetFirstServiceByName(string name)
        {
            ServiceEntity entityToReturn = Services.FirstOrDefault(n => n.Name == name);
            if (entityToReturn == null)
            {
                throw new ServiceNotFoundException();
            }
            return entityToReturn;
        }

        public ICollection<ServiceEntity> GetAllVisibleServices()
        {
            ICollection<ServiceEntity> entityToReturn = Services.Where(n => n.Status == Status.VisibleToAll).ToList();
            return entityToReturn;
        }

        public ICollection<ServiceEntity> GetAllVisibleToAdminServices()
        {
            ICollection<ServiceEntity> entityToReturn = Services.Where((ServiceEntity entity) => entity.Status == Status.VisibleToAdmin || entity.Status == Status.VisibleToAll).ToList();
            return entityToReturn;
        }

        public void Delete(ServiceEntity deleteEntity)
        {
            deleteEntity.Archive();
        }

        public void Save()
        {
            SaveChanges(true);
        }

        public void Save(bool acceptChangesOnSuccess)
        {
            SaveChanges(acceptChangesOnSuccess);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ServiceEntity>().HasIndex(b => b.Name).IsUnique();
            modelBuilder.Entity<ServiceEntity>()
                .Property(e => e.Status)
                .HasConversion<int>();

            base.OnModelCreating(modelBuilder);
        }
    }
}
