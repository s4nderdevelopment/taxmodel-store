﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TaxModel.EntityDTO.Entities;
using TaxModel.Exceptions.Admin;
using TaxModel.Interface.Repositories;

namespace TaxModel.Layers.BLL.Repositories
{
    public class AdminRepository : DbContext, IAdminRepository
    {
        protected DbSet<AdminEntity> Admins { get; set; }

        public AdminRepository(DbContextOptions<AdminRepository> options) : base(options)
        {
        }


        public ICollection<AdminEntity> GetAllAdmins()
        {
            ICollection<AdminEntity> entityToReturn = Admins.ToList();
            return entityToReturn;
        }

        public void Delete(ServiceEntity deleteEntity)
        {
            deleteEntity.Archive();
        }

        public void Save()
        {
            SaveChanges(true);
        }

        public AdminEntity GetByEmail(string email)
        {
           AdminEntity entityToReturn = Admins.FirstOrDefault(n => n.Email == email);
           return entityToReturn;
        }
		public AdminEntity Create(AdminEntity createEntity)
		{
			EntityEntry entry = Add(createEntity);
			if (entry.State != EntityState.Added)
			{
				throw new AdminCreateException();
			}
			AdminEntity entity = entry.Entity as AdminEntity;
			return entity;
		}
    }
}
