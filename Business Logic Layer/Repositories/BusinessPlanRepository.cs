﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Linq;
using TaxModel.EntityDTO.Entities;
using TaxModel.Exceptions.BusinessPlan;
using TaxModel.Interface.Converters;
using TaxModel.Interface.Repositories;

namespace TaxModel.Layers.BLL.Repositories
{
    public class BusinessPlanRepository : DbContext, IBusinessPlanRepository
    {
        protected DbSet<BusinessPlanEntity> BusinessPlans { get; set; }


        public BusinessPlanRepository(DbContextOptions<BusinessPlanRepository> options) : base(options)
        {

        }

        public BusinessPlanEntity CreateBusinessPlan(BusinessPlanEntity newEntity)
        {
            if (HasBusinessPlanWithName(newEntity.Name))
            {
                throw new BusinessPlanDuplicateNameException("name = " + newEntity.Name);
            }
            EntityEntry entry = Add(newEntity);
            if (entry.State != EntityState.Added)
            {
                throw new BusinessPlanCreateException();
            }

            BusinessPlanEntity createdEntity = entry.Entity as BusinessPlanEntity;

            return createdEntity;
        }

        public BusinessPlanEntity GetBusinessPlanByName(string name)
        {
            BusinessPlanEntity entity = BusinessPlans.FirstOrDefault(n => n.Name == name);
            if (entity == null)
            {
                throw new BusinessPlanNotFoundException();
            }
            return entity;
        }

        public BusinessPlanEntity GetBusinessPlanById(Guid id)
        {
            BusinessPlanEntity entity = BusinessPlans.FirstOrDefault(n => n.Id == id);
            if (entity == null)
            {
                throw new BusinessPlanNotFoundException();
            }
            return entity;
        }

        private bool HasBusinessPlanWithName(string name)
        {
            try
            {
                GetBusinessPlanByName(name);
                return true;
            }
            catch (BusinessPlanNotFoundException)
            {
                return false;
            }
        }


        public void Save()
        {
            SaveChanges(true);
        }
        public void Save(bool acceptChangesOnSuccess)
        {
            SaveChanges(acceptChangesOnSuccess);
        }

        //protected override void OnModelCreating(ModelBuilder builder)
        //{
        //    builder.Entity<BusinessPlanEntity>().HasIndex(bp => bp.Id).IsUnique();
        //}

    }
}
