﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Collections.Generic;
using System.Linq;
using TaxModel.EntityDTO.Entities;
using TaxModel.Exceptions.Client;
using TaxModel.Interface.Repositories;

namespace TaxModel.Layers.BLL.Repositories
{
    public class ClientRepository : DbContext, IClientRepository
    {
        protected DbSet<ClientEntity> Clients { get; set; }

        public ClientRepository(DbContextOptions<ClientRepository> options) : base(options)
        { }

        public ICollection<ClientEntity> GetAllClients()
        {
            ICollection<ClientEntity> entityToReturn = Clients.ToList();
            return entityToReturn;
        }

        public ClientEntity Create(ClientEntity newEntity)
        {
            EntityEntry entry = Add(newEntity);
            if (entry.State != EntityState.Added)
            {
                throw new ClientCreateException();
            }
            ClientEntity entity = entry.Entity as ClientEntity;
            return entity;
        }

        public ClientEntity GetByEmail(string email)
        {
            ClientEntity entityToReturn = Clients.FirstOrDefault(n => n.Email == email);
            return entityToReturn;
        }

        public void Save()
        {
            SaveChanges(true);
        }
    }
}