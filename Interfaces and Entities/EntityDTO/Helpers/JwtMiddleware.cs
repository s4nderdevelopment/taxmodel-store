﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using TaxModel.Interface.Services;

namespace TaxModel.EntityDTO.Helpers
{
    public class JwtMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly AppSettings _appSettings;

        public JwtMiddleware(RequestDelegate next, IOptions<AppSettings> appSettings)
        {
            _next = next;
            _appSettings = appSettings.Value;
        }

        public async Task Invoke(HttpContext context, IClientService clientService, IAdminService adminService)
        {
            var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();

            if (token != null)
                attachUserToContext(context, clientService, adminService, token);

            await _next(context);
        }

        private void attachUserToContext(HttpContext context, IClientService clientService, IAdminService adminService, string token)
        {
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            SecurityToken validatedToken = null;
            try
            {
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                    ClockSkew = TimeSpan.Zero
                }, out validatedToken);
            }
            catch (Exception)
            {
                return;
            }
            if (validatedToken == null)
            {
                return;
            }
            var jwtToken = (JwtSecurityToken)validatedToken;
            var UserEmail = (jwtToken.Claims.First(x => x.Type == "email").Value).ToString();
          
            //TODO: fix null reference service getbyemail


            //attach client to context on successful jwt validation

            try
            {
                context.Items["Admin"] = adminService.GetByEmail(UserEmail);
            }
            catch (Exception)
            {
                try     // to prevent user get triggering catch if left empty
                {
                    context.Items["Client"] = clientService.GetByEmail(UserEmail);
                }
                catch (Exception)
                {

                }
            }
        }
    }
}
