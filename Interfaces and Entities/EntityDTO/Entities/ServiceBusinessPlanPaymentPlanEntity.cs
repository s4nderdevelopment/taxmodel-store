﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TaxModel.EntityDTO.Entities
{
    [Table("BusinessPlanService")]
    public class ServiceBusinessPlanPaymentPlanEntity
    {
        [Key]
        [Column("BusinessPlanServiceID", TypeName = "nvarchar(36)")]
        public Guid Id { get; private set; }
        [Column("ServiceID", TypeName = "nvarchar(36)")]
        public Guid Service { get; private set; }
        [Column("BusinessPlanID", TypeName = "nvarchar(36)")]
        public Guid BusinessPlan { get; private set; }
        //[ForeignKey("PaymentPlan")]
        //[Column("PaymentPlanID", TypeName = "nvarchar(36)")]
        //public PaymentPlanEntity PaymentPlan { get; private set; }

        public ServiceBusinessPlanPaymentPlanEntity()
        {

        }

        public ServiceBusinessPlanPaymentPlanEntity(Guid service, Guid businessPlan)
        {
            Service = service;
            BusinessPlan = businessPlan;
        }
    }
}
