﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TaxModel.EntityDTO.Enums;

namespace TaxModel.EntityDTO.Entities
{
    [Table("BusinessPlan")]
    public class BusinessPlanEntity
    {
        [Key]
        [Column("BusinessPlanID", TypeName = "nvarchar(36)")]
        public Guid Id { get; private set; }
        [Column(TypeName = "nvarchar(255)")]
        public string Name { get; private set; }
        [Column(TypeName = "nvarchar(255)")]
        public string DisplayName { get; set; }
        [Column(TypeName = "text")]
        public string Description { get; set; }
        [Column(TypeName = "decimal(7, 2)")]
        public decimal Price { get; set; }
        [Column("Status", TypeName = "true false char as boolean")] //nog toevoegen aan database
        public Status Status { get; set; }

        public BusinessPlanEntity()
        {

        }


        public BusinessPlanEntity(string name, string displayName, string description, decimal price)
        {
            Name = name;
            DisplayName = displayName;
            Description = description;
            Price = price;
        }

        public BusinessPlanEntity(string name, string displayName, string description, decimal price, Status status)
        {
            Name = name;
            DisplayName = displayName;
            Description = description;
            Price = price;
            Status = status;
        }
    }
}
