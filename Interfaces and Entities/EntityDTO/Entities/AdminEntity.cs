﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TaxModel.EntityDTO.Entities
{
    [Table("Admin")]
    public class AdminEntity
    {
        [Key]
        [Column("AdminID", TypeName = "nvarchar(36)")]
        public Guid AdminId { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string Email { get; set; }
        [Column("Password", TypeName = "nvarchar(255)")]
        public string PasswordHash { get; set; }
        [Column("IsPrivileged", TypeName = "true false char as boolean")]
        public bool Privileged { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string Name { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string Salt { get; set; }

        public AdminEntity(string email, string passwordHash, bool privileged, string name)
        {
            this.Name = name;
            this.Email = email;
            this.PasswordHash = passwordHash;
            this.Privileged = privileged;
        }
     
        public AdminEntity()
        {

        }
    }


}
