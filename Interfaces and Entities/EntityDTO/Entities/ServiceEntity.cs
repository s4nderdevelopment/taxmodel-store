﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TaxModel.EntityDTO.Enums;

namespace TaxModel.EntityDTO.Entities
{
    [Table("Service")]
    public class ServiceEntity
    {
        [Key]
        [Column("ServiceID", TypeName = "nvarchar(36)")]
        public Guid Id { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string Name { get; private set; }
        [Column(TypeName = "nvarchar(255)")]
        public string DisplayName { get; private set; }
        [Column(TypeName = "text")]
        public string Description { get; private set; }

        [Column("Status", TypeName = "int")]
        public Status Status { get; private set; }



        public ServiceEntity()
        {



        }

        public ServiceEntity(string name, string displayName, string description, Status status)
        {
            Name = name;
            DisplayName = displayName;
            Description = description;
            Status = status;
        }

        public void Update(ServiceEntity entity)
        {
            DisplayName = entity.DisplayName;
            Description = entity.Description;
            Status = entity.Status;
        }

        public void Archive()
        {
            Status = Status.Invisible;
        }
    }
}
