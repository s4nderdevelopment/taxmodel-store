﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TaxModel.EntityDTO.Entities
{
    [Table("Client")]
    public class ClientEntity
    {

        [Key]
        [Column("ClientID", TypeName = "nvarchar(36)")]
        public Guid ClientId { get; private set; }
        [Column("Password", TypeName = "nvarchar(255)")]
        public string PasswordHash { get; private set; }
        [Column(TypeName = "nvarchar(255)")]
        public string Salt { get; set; }
        [Column(TypeName = "nvarchar(255)")]
        public string Email { get; private set; }
        [Column(TypeName = "nvarchar(100)")]
        public string Name { get; private set; }
        [Column(TypeName = "nvarchar(100)")]
        public string PhoneNumber { get; private set; }
        [Column(TypeName = "varchar(100)")]
        public string CompanyName { get; private set; }


        public ClientEntity(string name, string password, string email, string phoneNumber, string companyName)
        {
            this.Name = name;
            this.PasswordHash = password;
            this.Email = email;
            this.PhoneNumber = phoneNumber;
            this.CompanyName = companyName;
        }

        public ClientEntity(Guid clientId, string name, string password, string email, string phoneNumber, string companyName, string salt)
        {
            this.ClientId = clientId;
            this.Name = name;
            this.PasswordHash = password;
            this.Email = email;
            this.PhoneNumber = phoneNumber;
            this.CompanyName = companyName;
            this.Salt = salt;
        }

        public ClientEntity()
        {

        }
    }
}
