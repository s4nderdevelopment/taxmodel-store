﻿using TaxModel.EntityDTO.Entities;

namespace TaxModel.Startup.Models
{
    public class AuthenticateResponseClient
    {
        public string Email { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string CompanyName { get; set; }
        public string Token { get; set; }

        public AuthenticateResponseClient(ClientEntity client, string token)
        {
            Email = client.Email;
            Name = client.Name;
            PhoneNumber = client.PhoneNumber;
            CompanyName = client.CompanyName;
            Token = token;
        }
    }
}
