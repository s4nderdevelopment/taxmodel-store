﻿using TaxModel.EntityDTO.Entities;

namespace TaxModel.Startup.Models
{
    public class AuthenticateResponseAdmin
    {
        public string Email { get; set; }
        public bool Privileged { get; set; }
        public string Token { get; set; }

        public AuthenticateResponseAdmin(AdminEntity admin, string token)
        {
            Email = admin.Email;
            Privileged = admin.Privileged;
            Token = token;
        }
    }
}
