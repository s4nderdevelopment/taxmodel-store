﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace TaxModel.EntityDTO.ViewModels.Account
{
    [JsonObject]
    public struct CreateClientRequestViewModel
    {
        [Required] public string Email { get; set; }
        [Required] public string Password { get; set; }
        [Required] public string Name { get; set; }
        [Required] public string CompanyName { get; set; }
        public string PhoneNumber { get; set; }
    }
}
