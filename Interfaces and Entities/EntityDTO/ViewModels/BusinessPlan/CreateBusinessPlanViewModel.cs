﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace TaxModel.TaxModel.EntityDTO.ViewModels.BusinessPlan
{
    [JsonObject]
    public struct CreateBusinessPlanViewModel
    {
        [Required]        
        public string DisplayName { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public decimal Price { get; set; }

    }
}
