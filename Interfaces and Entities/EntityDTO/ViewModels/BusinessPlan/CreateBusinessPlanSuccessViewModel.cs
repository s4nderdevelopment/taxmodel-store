﻿using Newtonsoft.Json;

namespace TaxModel.EntityDTO.ViewModels.BusinessPlan
{
    [JsonObject]
    public struct CreateBusinessPlanSuccessViewModel
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }

        public CreateBusinessPlanSuccessViewModel(string name, string displayName, string description, decimal price)
        {
            Name = name;
            DisplayName = displayName;
            Description = description;
            Price = price;
        }
    }
}
