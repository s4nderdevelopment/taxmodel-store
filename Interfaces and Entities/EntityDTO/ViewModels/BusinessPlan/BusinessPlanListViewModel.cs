﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace TaxModel.EntityDTO.ViewModels.BusinessPlan
{
    [JsonObject]
    public class BusinessPlanListViewModel : List<BusinessPlanViewModel>
    {
        public BusinessPlanListViewModel()
        {

        }

        public BusinessPlanListViewModel(ICollection<BusinessPlanViewModel> viewModels)
        {
            foreach (BusinessPlanViewModel viewModel in viewModels)
            {
                Add(viewModel);
            }
        }
    }
}
