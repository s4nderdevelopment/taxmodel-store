﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace TaxModel.EntityDTO.ViewModels
{
    [JsonObject]
    public class ErrorViewModel
    {
        [Required]        public string Error { get; set; }

        public ErrorViewModel()
        {
            Error = "An unknown error occurred";
        }

        public ErrorViewModel(string error)
        {
            Error = error;
        }
    }
}
