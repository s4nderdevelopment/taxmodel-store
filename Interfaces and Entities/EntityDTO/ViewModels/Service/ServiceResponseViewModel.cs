﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using TaxModel.EntityDTO.Enums;

namespace TaxModel.Startup.ViewModels.Service
{
    [JsonObject]
    public struct ServiceResponseViewModel
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string DisplayName { get; set; }
        [Required]
        public string Description { get; set; }
        [Required]
        public Status Status { get; set; }

        public ServiceResponseViewModel(string name, string displayName, string description, Status status)
        {
            Name = name;
            DisplayName = displayName;
            Description = description;
            Status = status;
        }
    }
}
