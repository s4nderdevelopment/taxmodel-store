﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;
using TaxModel.EntityDTO.Enums;

namespace TaxModel.EntityDTO.ViewModels.Service
{
    [JsonObject]
    public struct UpdateServiceRequestViewModel
    {
        [Required]
        public string DisplayName { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public Status Status { get; set; }

        [Required]
        public string Name { get; set; }
    }
}
