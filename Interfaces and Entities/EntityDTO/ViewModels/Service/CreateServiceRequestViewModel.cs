﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace TaxModel.Startup.ViewModels.Service
{
    [JsonObject]
    public struct CreateServiceRequestViewModel
    {
        [Required]
        public string DisplayName { get; set; }

        [Required]
        public string Description { get; set; }

    }
}
