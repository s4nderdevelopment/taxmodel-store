﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Newtonsoft.Json;

namespace TaxModel.EntityDTO.ViewModels.Admin
{
    [JsonObject]
    public struct CreateAdminRequestViewModel
    {
        [Required]
        public string Email { get; set; }
        [Required]
        [StringLength(30, MinimumLength = 8)]
        public string Password { get; set; }
        [Required]
        public bool Privileged { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
