﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Newtonsoft.Json;

namespace TaxModel.EntityDTO.ViewModels.Admin
{
    [JsonObject]
    public struct AdminResponseViewModel
    {
        public Guid AdminId { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public bool Privileged { get; set; }
        [Required]
        public string Name { get; set; }
    }
}
