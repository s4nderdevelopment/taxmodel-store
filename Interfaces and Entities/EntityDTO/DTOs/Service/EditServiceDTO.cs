﻿using TaxModel.EntityDTO.Enums;

namespace TaxModel.EntityDTO.DTOs.Service
{
    public struct EditServiceDTO
    {
        public string DisplayName { get; set; }
        public string Name { get; }
        public string Description { get; set; }
        public Status Status { get; set; }

        public EditServiceDTO(string displayName, string description, string name, Status status)
        {
            DisplayName = displayName;
            Description = description;
            Name = name;
            Status = status;
        }
    }
}
