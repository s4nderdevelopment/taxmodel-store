﻿using TaxModel.EntityDTO.Enums;

namespace TaxModel.EntityDTO.DTOs.Service
{
    public struct ServiceDTO
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public Status Status { get; set; }

        public ServiceDTO(string name, string displayName, string description, Status isActivated)
        {
            Name = name;
            DisplayName = displayName;
            Description = description;
            Status = isActivated;
        }
    }
}
