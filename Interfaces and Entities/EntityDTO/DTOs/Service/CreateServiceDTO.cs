﻿namespace TaxModel.EntityDTO.DTOs.Service
{
    public struct CreateServiceDTO
    {
        public string DisplayName { get; set; }
        public string Description { get; set; }

        public CreateServiceDTO(string displayName, string description)
        {
            DisplayName = displayName;
            Description = description;
        }
    }
}
