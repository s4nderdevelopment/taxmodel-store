﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaxModel.EntityDTO.DTOs.Admin
{
    public struct CreateAdminDTO
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public bool Privileged { get; set; }
        public string Name { get; set; }
    }
}
