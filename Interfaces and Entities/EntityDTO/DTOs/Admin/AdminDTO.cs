﻿using System;

namespace TaxModel.EntityDTO.DTOs.Admin
{
    public struct AdminDTO
    {
        public Guid AdminId { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public bool Privileged { get; set; }

        public AdminDTO(Guid adminId, string email, string name, bool privileged)
        {
            AdminId = adminId;
            Email = email;
            Name = name;
            Privileged = privileged;
        }
    }
}
