﻿namespace TaxModel.EntityDTO.DTOs.Client
{
    public struct CreateClientDTO
    {
        public string Email { get; set; }
        public string Password { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string CompanyName { get; set; }
    }
}
