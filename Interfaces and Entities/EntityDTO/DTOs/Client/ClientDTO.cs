﻿using System;

namespace TaxModel.EntityDTO.DTOs.Client
{
    public struct ClientDTO
    {

        public Guid ClientId { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string CompanyName { get; set; }
    }
}
