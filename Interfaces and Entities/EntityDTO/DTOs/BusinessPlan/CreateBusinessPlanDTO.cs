﻿namespace TaxModel.EntityDTO.DTOs.BusinessPlan
{
    public struct CreateBusinessPlanDTO
    {
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }

        public CreateBusinessPlanDTO(string displayName, string description, decimal price)
        {
            DisplayName = displayName;
            Description = description;
            Price = price;
        }
    }
}
