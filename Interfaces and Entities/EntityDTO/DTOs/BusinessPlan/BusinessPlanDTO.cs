﻿using TaxModel.EntityDTO.Enums;

namespace TaxModel.EntityDTO.DTOs.BusinessPlan
{
    public struct BusinessPlanDTO
    {
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Description { get; set; }
        public decimal Price { get; set; }
        public Status Status { get; set; }

        public BusinessPlanDTO(string name, string displayName, string description, decimal price, Status status)
        {
            Name = name;
            DisplayName = displayName;
            Description = description;
            Price = price;
            Status = status;
        }
    }
}
