﻿namespace TaxModel.EntityDTO.Enums
{
    public enum Status
    {
        VisibleToAll,
        VisibleToAdmin,
        Invisible
    }
}
