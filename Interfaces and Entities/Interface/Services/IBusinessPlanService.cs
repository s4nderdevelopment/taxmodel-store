﻿using System.Collections.Generic;
using TaxModel.EntityDTO.DTOs.BusinessPlan;

namespace TaxModel.Interface.Services
{
    public interface IBusinessPlanService
    {
        BusinessPlanDTO CreateBusinessPlan(CreateBusinessPlanDTO createDTO);

        BusinessPlanDTO EditBusinessPlan(EditBusinessPlanDTO editDTO);

        BusinessPlanDTO GetBusinessPlanByName(string name);

        ICollection<BusinessPlanDTO> GetBusinessPlansByServiceName(string serviceName);
    }
}
