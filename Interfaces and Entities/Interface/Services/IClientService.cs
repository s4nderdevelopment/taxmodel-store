﻿using System;
using System.Collections.Generic;
using TaxModel.EntityDTO.DTOs.Client;
using TaxModel.EntityDTO.Entities;
using TaxModel.Startup.Models;

namespace TaxModel.Interface.Services
{
    public interface IClientService
    {
        AuthenticateResponseClient Authenticate(AuthenticateRequest model);

        ICollection<ClientDTO> GetAllClients();

        ClientDTO GetByEmail(string email);

        ClientDTO Create(CreateClientDTO dto);

    }
}
