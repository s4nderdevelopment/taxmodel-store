﻿using System;
using System.Collections.Generic;
using TaxModel.EntityDTO.DTOs.Admin;
using TaxModel.EntityDTO.Entities;
using TaxModel.Startup.Models;

namespace TaxModel.Interface.Services
{
    public interface IAdminService
    {
        AuthenticateResponseAdmin Authenticate(AuthenticateRequest model);

        ICollection<AdminDTO> GetAllAdmins();

        AdminDTO GetByEmail(string email);
        
        AdminDTO Create(CreateAdminDTO dto);
    }
}
