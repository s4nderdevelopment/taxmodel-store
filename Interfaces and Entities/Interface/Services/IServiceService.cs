﻿using System.Collections.Generic;
using TaxModel.EntityDTO.DTOs.Service;

namespace TaxModel.Interface.Services
{
    public interface IServiceService
    {
        ServiceDTO CreateService(CreateServiceDTO InsertPresentationServiceDTO);

        ServiceDTO Update(EditServiceDTO dto);

        ServiceDTO GetServiceByName(string name);

        bool Delete(string name);

        ICollection<ServiceDTO> GetAllVisibleServices();
        ICollection<ServiceDTO> GetAllVisibleToAdminServices();
    }
}
