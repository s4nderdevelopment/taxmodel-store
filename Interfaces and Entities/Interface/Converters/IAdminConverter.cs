﻿using System;
using System.Collections.Generic;
using System.Text;
using TaxModel.EntityDTO.DTOs.Admin;
using TaxModel.EntityDTO.Entities;
using TaxModel.EntityDTO.ViewModels.Admin;

namespace TaxModel.Interface.Converters
{
    public interface IAdminConverter
    {
        CreateAdminDTO ConvertToCreateAdminDTO(CreateAdminRequestViewModel vm);

        AdminResponseViewModel ConvertToResponseVm(AdminDTO dto);

        ICollection<AdminDTO> ConvertToDTOs(ICollection<AdminEntity> entities);

        AdminEntity ConvertToAdminEntity(CreateAdminDTO dto);

        AdminDTO ConvertToAdminDTO(AdminEntity entity);
    }
}