﻿using System.Collections.Generic;
using TaxModel.EntityDTO.DTOs.Client;
using TaxModel.EntityDTO.Entities;
using TaxModel.EntityDTO.ViewModels.Account;
using TaxModel.EntityDTO.ViewModels.Client;

namespace TaxModel.Interface.Converters
{
    public interface IClientConverter
    {
        ClientDTO ConvertToClientDTO(ClientEntity entity);
        CreateClientDTO ConvertToCreateClientDTO(CreateClientRequestViewModel vm);
        ClientEntity ConvertToClientEntity(CreateClientDTO dto);
        ClientResponseViewModel ConvertToResponseVm(ClientDTO dto);
        ICollection<ClientDTO> ConvertToDTOs(ICollection<ClientEntity> entities);
    }
}
