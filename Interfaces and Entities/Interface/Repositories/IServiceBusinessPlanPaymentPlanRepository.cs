﻿using System;
using System.Collections.Generic;
using TaxModel.EntityDTO.Entities;

namespace TaxModel.Interface.Repositories
{
    public interface IServiceBusinessPlanPaymentPlanRepository
    {
        ICollection<ServiceBusinessPlanPaymentPlanEntity> GetByBusinessPlanName(Guid businessPlanId);
        ICollection<ServiceBusinessPlanPaymentPlanEntity> GetByServiceName(Guid serviceId);

        void Save();
        void Save(bool acceptChangesOnSuccess);
    }
}
