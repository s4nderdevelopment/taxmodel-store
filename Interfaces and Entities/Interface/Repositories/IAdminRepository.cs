﻿using System.Collections.Generic;
using TaxModel.EntityDTO.Entities;

namespace TaxModel.Interface.Repositories
{
    public interface IAdminRepository
    {
        ICollection<AdminEntity> GetAllAdmins();
        void Delete(ServiceEntity deleteEntity);
        void Save();
        AdminEntity GetByEmail(string email);
        AdminEntity Create(AdminEntity createEntity);
    }
}
