﻿using System;
using TaxModel.EntityDTO.Entities;

namespace TaxModel.Interface.Repositories
{
    public interface IBusinessPlanRepository
    {
        BusinessPlanEntity CreateBusinessPlan(BusinessPlanEntity entity);

        BusinessPlanEntity GetBusinessPlanByName(string name);

        BusinessPlanEntity GetBusinessPlanById(Guid id);

        void Save();
        void Save(bool acceptChangesOnSuccess);
    }
}
