﻿using System.Collections.Generic;
using TaxModel.EntityDTO.Entities;

namespace TaxModel.Interface.Repositories
{
    public interface IClientRepository
    {
        ICollection<ClientEntity> GetAllClients();
        ClientEntity Create(ClientEntity createEntity);
        ClientEntity GetByEmail(string email);
        void Save();
    }
}