﻿using System;
using System.Collections.Generic;
using TaxModel.EntityDTO.Entities;

namespace TaxModel.Interface.Repositories
{
    public interface IServiceRepository : IDisposable
    {
        ServiceEntity CreateService(ServiceEntity createEntity);

        ServiceEntity GetFirstServiceByName(string name);
        ICollection<ServiceEntity> GetAllVisibleServices();
        ServiceEntity UpdateService(ServiceEntity updateEntity);
        ICollection<ServiceEntity> GetAllVisibleToAdminServices();
        void Delete(ServiceEntity deleteEntity);
        void Save();
        void Save(bool acceptChangesOnSuccess);
    }
}
