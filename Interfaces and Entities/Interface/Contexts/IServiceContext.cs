﻿using System.Collections.Generic;
using TaxModel.EntityDTO.DTOs.Service;

namespace TaxModel.Interface.Contexts
{
    public interface IServiceContext
    {
        ServiceDTO CreateService(CreateServiceDTO createDTO);

        ServiceDTO GetFirstServiceByName(string name);
        ICollection<ServiceDTO> GetServicesByNames(ICollection<string> names);
        ICollection<ServiceDTO> GetAllVisibleServices();

        void Save();
        void Save(bool acceptChangesOnSuccess);
    }
}
