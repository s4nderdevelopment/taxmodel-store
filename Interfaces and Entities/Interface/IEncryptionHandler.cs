﻿namespace TaxModel.Interface
{
    public interface IEncryptionHandler
    {
        string HashPassword(string password, out string salt);

        bool Verify(string inputPassword, string hashedPassword);
    }
}
