using AutoMapper;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.SpaServices.ReactDevelopmentServer;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using TaxModel.EntityDTO.Helpers;
using TaxModel.Interface;
using TaxModel.Interface.Converters;
using TaxModel.Interface.Repositories;
using TaxModel.Interface.Services;
using TaxModel.Layers.BLL;
using TaxModel.Layers.BLL.Repositories;
using TaxModel.Layers.BLL.Services;
using TaxModel.Startup.MappingProfiles;
using TaxModel.Startup.Models.Converters;

namespace TaxModel.Startup
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));

            services.AddScoped<IClientService, ClientService>();
            services.AddScoped<IAdminService, AdminService>();
            services.AddAutoMapper(typeof(MappingProfile));
            services.AddScoped<IEncryptionHandler, BCryptHandler>();

            #region Service DI
            services.AddScoped<IServiceService, ServiceService>();
            services.AddScoped<IServiceRepository, ServiceRepository>();
            services.AddDbContext<ServiceRepository>((optionsBuilder) =>
            {
                optionsBuilder.UseMySQL(Configuration.GetConnectionString("sqlConnString"));
            });
            #endregion

            #region BusinessPlan DI
            services.AddScoped<IBusinessPlanService, BusinessPlanService>();
            services.AddScoped<IBusinessPlanRepository, BusinessPlanRepository>();

            services.AddDbContext<BusinessPlanRepository>((optionsBuilder) =>
            {
                optionsBuilder.UseMySQL(Configuration.GetConnectionString("sqlConnString"));
            });
            #endregion

            #region Client DI
            services.AddScoped<IClientService, ClientService>();
            services.AddScoped<IClientConverter, ClientConverter>();
            services.AddScoped<IClientRepository, ClientRepository>();
            
            services.AddDbContext<ClientRepository>((optionsBuilder) =>
            {
                optionsBuilder.UseMySQL(Configuration.GetConnectionString("sqlConnString"));
            });
            #endregion

            #region Admin DI

            services.AddScoped<IAdminService, AdminService>();
            services.AddScoped<IAdminRepository, AdminRepository>();
            services.AddScoped<IAdminConverter, AdminConverter>();
            services.AddDbContext<AdminRepository>((optionsBuilder) =>
            {
                optionsBuilder.UseMySQL(Configuration.GetConnectionString("sqlConnString"));
            });
            #endregion

            services.AddScoped<IServiceBusinessPlanPaymentPlanRepository, ServiceBusinessPlanPaymentPlanRepository>();
            services.AddDbContext<ServiceBusinessPlanPaymentPlanRepository>((optionsBuilder) =>
            {
                optionsBuilder.UseMySQL(Configuration.GetConnectionString("sqlConnString"));
            });

            services.AddControllersWithViews();

            // In production, the React files will be served from this directory
            services.AddSpaStaticFiles(configuration =>
            {
                configuration.RootPath = "ClientApp/build";
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseSpaStaticFiles();

            app.UseRouting();
            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseMiddleware<JwtMiddleware>();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller}/{action=Index}/{id?}");
            });

            app.UseSpa(spa =>
            {
                spa.Options.SourcePath = "ClientApp";

                if (env.IsDevelopment())
                {
                    spa.UseReactDevelopmentServer(npmScript: "start");
                }
            });
        }
    }
}
