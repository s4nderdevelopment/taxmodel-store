﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using TaxModel.EntityDTO.DTOs.BusinessPlan;
using TaxModel.EntityDTO.DTOs.Client;
using TaxModel.EntityDTO.DTOs.Service;
using TaxModel.EntityDTO.Entities;
using TaxModel.EntityDTO.ViewModels.Account;
using TaxModel.EntityDTO.ViewModels.BusinessPlan;
using TaxModel.EntityDTO.ViewModels.Client;
using TaxModel.EntityDTO.ViewModels.Service;
using TaxModel.Startup.ViewModels.Service;
using TaxModel.TaxModel.EntityDTO.ViewModels.BusinessPlan;

namespace TaxModel.Startup.MappingProfiles
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            //Service Mappers
            CreateMap<CreateServiceRequestViewModel, CreateServiceDTO>();
            CreateMap<CreateServiceDTO, ServiceEntity>().ForMember((dest) => dest.Name, (opt) =>
                opt.MapFrom((src) => GenerateName(src.DisplayName)));
            CreateMap<ServiceEntity, ServiceDTO>();
            CreateMap<ServiceDTO, ServiceResponseViewModel>();
            CreateMap<ServiceResponseViewModel, ServiceDTO>();
            CreateMap<UpdateServiceRequestViewModel, EditServiceDTO>();
            CreateMap<EditServiceDTO, ServiceEntity>();

            //Businessplan Mappers
            CreateMap<CreateBusinessPlanViewModel, CreateBusinessPlanDTO>();
            CreateMap<CreateBusinessPlanDTO, BusinessPlanEntity>().ForMember((dest) => dest.Name, (opt) =>
                opt.MapFrom((src) => GenerateName(src.DisplayName)));
            CreateMap<BusinessPlanEntity, BusinessPlanDTO>();
            CreateMap<BusinessPlanDTO, BusinessPlanViewModel>();
            CreateMap<EditBusinessPlanViewModel, EditBusinessPlanDTO>();
            CreateMap<EditBusinessPlanDTO, BusinessPlanEntity>();

            //Client Mappers
            CreateMap<CreateClientRequestViewModel, ClientDTO>();
            CreateMap<CreateClientDTO, ClientEntity>();
            CreateMap<ClientEntity, ClientDTO>();
            CreateMap<ClientDTO, ClientResponseViewModel>();

        }

        private string GenerateName(string displayName)
        {
            // Removes all not allowed characters. Allowed characters are lowercase a-z, 0-9 and a dash (-).
            string name = displayName.Trim().ToLower();
            Regex rgx = new Regex("[^a-zA-Z0-9 ]");
            name = rgx.Replace(name, "").Trim();
            return name.Replace(" ", "-") + "-" + Guid.NewGuid().ToString();
        }
    }
}
