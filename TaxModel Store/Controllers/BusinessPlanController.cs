﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Net.Mime;
using TaxModel.EntityDTO.DTOs.BusinessPlan;
using TaxModel.EntityDTO.Enums;
using TaxModel.EntityDTO.Helpers;
using TaxModel.EntityDTO.ViewModels;
using TaxModel.EntityDTO.ViewModels.BusinessPlan;
using TaxModel.Exceptions.BusinessPlan;
using TaxModel.Interface.Converters;
using TaxModel.Interface.Services;
using TaxModel.TaxModel.EntityDTO.ViewModels.BusinessPlan;

namespace TaxModel.Startup.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BusinessPlanController : ControllerBase
    {
        private readonly IBusinessPlanService _businessPlanService;
        private readonly IMapper _mapper;
        private readonly ILogger<BusinessPlanController> _logger;

        public BusinessPlanController(IBusinessPlanService businessPlanService, IMapper mapper, ILogger<BusinessPlanController> logger)
        {
            _businessPlanService = businessPlanService;
            _mapper = mapper;
            _logger = logger;
        }

        [Route("create")]
        [HttpPost]
        [AdminAuthorize]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Create([FromBody] CreateBusinessPlanViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ErrorViewModel("Bad request"));
            }

            CreateBusinessPlanDTO createDTO = _mapper.Map<CreateBusinessPlanDTO>(viewModel);
            BusinessPlanDTO dto = _businessPlanService.CreateBusinessPlan(createDTO);

            Response.StatusCode = StatusCodes.Status201Created;
            return Ok(_mapper.Map<BusinessPlanViewModel>(dto));
        }

        [Route("getByName")]
        [HttpGet]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult GetByName([FromQuery(Name = "name")] string name)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new ErrorViewModel("Bad request"));
                }
                BusinessPlanDTO dto = _businessPlanService.GetBusinessPlanByName(name);
                if (dto.Status == Status.VisibleToAdmin || dto.Status == Status.Invisible)
                {
                    if (HttpContext.Items["Admin"] == null)
                    {
                        return Unauthorized(new ErrorViewModel("Unauthorized"));
                    }
                }

                return Ok(new BusinessPlanViewModel(dto.Name, dto.DisplayName, dto.Description, dto.Price, dto.Status));
            }
            catch (BusinessPlanNotFoundException)
            {
                return NotFound(new ErrorViewModel("Business plan not found!"));
            }
        }

        [Route("getByService")]
        [HttpGet]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult GetByService([FromQuery] string serviceName)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ErrorViewModel("Bad request"));
            }

            ICollection<BusinessPlanDTO> dtos = _businessPlanService.GetBusinessPlansByServiceName(serviceName);

            BusinessPlanListViewModel businessPlanListViewModel = new BusinessPlanListViewModel();

            foreach (BusinessPlanDTO dto in dtos)
            {
                businessPlanListViewModel.Add(new BusinessPlanViewModel(dto.Name, dto.DisplayName, dto.Description, dto.Price, dto.Status));
            }

            return Ok(businessPlanListViewModel);
        }

        [Route("update")]
        [HttpPost]
        [AdminAuthorize]
        [Consumes(MediaTypeNames.Application.Json)]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Update([FromBody] EditBusinessPlanViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ErrorViewModel("Bad request"));
            }

            EditBusinessPlanDTO editDTO = _mapper.Map<EditBusinessPlanDTO>(viewModel);
            BusinessPlanDTO dto = _businessPlanService.EditBusinessPlan(editDTO);

            Response.StatusCode = StatusCodes.Status201Created;
            return Ok(_mapper.Map<BusinessPlanViewModel>(dto));
        }
    }
}
