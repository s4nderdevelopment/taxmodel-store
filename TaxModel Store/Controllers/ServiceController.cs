﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Net.Mime;
using TaxModel.EntityDTO.DTOs.Service;
using TaxModel.EntityDTO.Enums;
using TaxModel.EntityDTO.Helpers;
using TaxModel.EntityDTO.ViewModels;
using TaxModel.EntityDTO.ViewModels.Service;
using TaxModel.Exceptions.Service;
using TaxModel.Interface.Converters;
using TaxModel.Interface.Services;
using TaxModel.Startup.ViewModels.Service;

namespace TaxModel.Startup.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ServiceController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IServiceService _serviceService;

        private readonly ILogger<ServiceController> _logger;

        public ServiceController(IMapper mapper, IServiceService serviceService, ILogger<ServiceController> logger)
        {
            _mapper = mapper;
            _serviceService = serviceService;
            _logger = logger;
        }

        [Route("create")]
        [HttpPost]
        [AdminAuthorize]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult CreateService([FromBody] CreateServiceRequestViewModel service)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ErrorViewModel("Bad request"));
            }

            ServiceDTO dto = _serviceService.CreateService(_mapper.Map<CreateServiceDTO>(service));
            ServiceResponseViewModel viewModel = _mapper.Map<ServiceResponseViewModel>(dto);
            return Ok(viewModel);
        }

        [Route("getByName")]
        [HttpGet]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult GetByName([FromQuery(Name = "name")] string name)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    return BadRequest(new ErrorViewModel("Bad request"));
                }

                ServiceDTO dto = _serviceService.GetServiceByName(name);
                if (dto.Status == Status.VisibleToAdmin || dto.Status == Status.Invisible)
                {
                    if (HttpContext.Items["Admin"] == null)
                    {
                        return Unauthorized(new ErrorViewModel("Unauthorized"));
                    }
                }

                return Ok(_mapper.Map<ServiceResponseViewModel>(dto));
            }
            catch (ServiceNotFoundException)
            {
                return NotFound(new ErrorViewModel("Service not found!"));
            }
        }

        [Route("getAllVisibleServices")]
        [HttpGet]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult GetAllVisibleServices()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ErrorViewModel("Bad request"));
            }

            ICollection<ServiceResponseViewModel> allServiceList = _mapper.Map<ICollection<ServiceResponseViewModel>>(_serviceService.GetAllVisibleServices());

            return Ok(allServiceList);
        }

        [Route("update")]
        [HttpPost]
        [AdminAuthorize]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Update([FromBody] UpdateServiceRequestViewModel viewModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ErrorViewModel("Bad request"));
            }

            ServiceDTO dto = _serviceService.Update(_mapper.Map<EditServiceDTO>(viewModel));
            return Ok(_mapper.Map<ServiceResponseViewModel>(dto));
        }

        [Route("archive")]
        [HttpPost]
        [AdminAuthorize]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult ArchiveServices([FromQuery(Name = "name")] string name)
        {
            _serviceService.Delete(name);

            return Ok(true);
        }

        [Route("getAllVisibleToAdminServices")]
        [HttpGet]
        [AdminAuthorize]
        [Produces(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public IActionResult GetAllVisibleToAdminServices()
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(new ErrorViewModel("Bad request"));
            }

            ICollection<ServiceResponseViewModel> allServiceList = _mapper.Map<ICollection<ServiceResponseViewModel>>(_serviceService.GetAllVisibleToAdminServices());

            return Ok(allServiceList);
        }

    }
}
