﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
﻿using System.Net.Mime;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Net.Mime;
using TaxModel.EntityDTO.Helpers;
using Microsoft.Extensions.Logging;
using TaxModel.EntityDTO.DTOs.Admin;
using TaxModel.EntityDTO.DTOs.Client;
using TaxModel.EntityDTO.ViewModels;
using TaxModel.EntityDTO.ViewModels.Account;
using TaxModel.EntityDTO.ViewModels.Admin;
using TaxModel.EntityDTO.ViewModels.Client;
using TaxModel.Exceptions;
using TaxModel.Interface.Converters;
using TaxModel.Interface.Services;
using TaxModel.Startup.Models;

namespace TaxModel.Startup.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AdminController : ControllerBase
    {
        private readonly IAdminService _adminService;
        private readonly IAdminConverter _adminConverter;
        private readonly ILogger<ServiceController> _logger;

        public AdminController(IAdminService adminService,IAdminConverter adminConverter, ILogger<ServiceController> logger)
        {
            _adminService = adminService;
            _adminConverter = adminConverter;
            _logger = logger;
        }

        [HttpPost("login")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Login(AuthenticateRequest model)
        {
            var response = _adminService.Authenticate(model);

            if (response == null)
                return BadRequest(new { message = "Username or password is incorrect" });
            return Ok(response);
        }
        
        [AdminAuthorize]
        [HttpGet]
        public IActionResult GetAll()
        {
            var admins = _adminService.GetAllAdmins();
            return Ok(admins);
        }

        [Route("create")]
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public object Create([FromBody] CreateAdminRequestViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    throw new InvalidModelStateException("Invalid admin model");
                }
                AdminDTO adminDto = _adminService.Create(_adminConverter.ConvertToCreateAdminDTO(viewModel));
                AdminResponseViewModel responseVm = _adminConverter.ConvertToResponseVm(adminDto);
                Response.StatusCode = StatusCodes.Status201Created;
                return responseVm;
            }
            catch (InvalidModelStateException e)
            {
                _logger.LogError(e, "Invalid model state");
                Response.StatusCode = StatusCodes.Status400BadRequest;
                return new ErrorViewModel("Bad request");
            }
        }
    }
}
