﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Net.Mime;
using Microsoft.AspNetCore.Authorization;
using TaxModel.EntityDTO.DTOs.Client;
using TaxModel.EntityDTO.ViewModels;
using TaxModel.EntityDTO.ViewModels.Account;
using TaxModel.EntityDTO.ViewModels.Client;
using TaxModel.Exceptions;
using TaxModel.Interface.Converters;
using TaxModel.Interface.Services;
using TaxModel.Startup.Models;
using TaxModel.EntityDTO.Helpers;

namespace TaxModel.Startup.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientController : ControllerBase
    {
        private readonly IClientService _clientService;
        private readonly ILogger<ServiceController> _logger;
        private readonly IMapper _mapper;

        public ClientController(IClientService clientService, ILogger<ServiceController> logger, IMapper mapper)
        {
            _clientService = clientService;
            _logger = logger;
            _mapper = mapper;
        }
        
            
        [HttpPost("login")]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public IActionResult Login(AuthenticateRequest model)
        {
            var response = _clientService.Authenticate(model);

            if (response == null)
                return BadRequest(new { message = "Username or password is incorrect" });

            return Ok(response);
        }

        public IActionResult GetAllClients()
        {
            var users = _clientService.GetAllClients();
            return Ok(users);
        }

        [Route("create")]
        [HttpPost]
        [Consumes(MediaTypeNames.Application.Json)]
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public object Create([FromBody] CreateClientRequestViewModel viewModel)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    throw new InvalidModelStateException("Invalid client model");
                }

                ClientDTO clientDto = _clientService.Create(_mapper.Map<CreateClientDTO>(viewModel));
                ClientResponseViewModel responseVm = _mapper.Map<ClientResponseViewModel>(clientDto);
                Response.StatusCode = StatusCodes.Status201Created;
                return responseVm;
            }
            catch (InvalidModelStateException e)
            {
                _logger.LogError(e, "Invalid model state");
                Response.StatusCode = StatusCodes.Status400BadRequest;
                return new ErrorViewModel("Bad request");
            }
        }
    }
}
