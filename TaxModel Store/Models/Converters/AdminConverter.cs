﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Transactions;
using TaxModel.EntityDTO.DTOs.Admin;
using TaxModel.EntityDTO.Entities;
using TaxModel.EntityDTO.ViewModels.Admin;
using TaxModel.Interface.Converters;

namespace TaxModel.Startup.Models.Converters
{
    public class AdminConverter : IAdminConverter
    {
        public CreateAdminDTO ConvertToCreateAdminDTO(CreateAdminRequestViewModel vm)
        {
            return new CreateAdminDTO
            {
                Name = vm.Name,
                Email = vm.Email,
                Password = vm.Password,
                Privileged = vm.Privileged
            };
        }


        public AdminResponseViewModel ConvertToResponseVm(AdminDTO dto)
        {
            return new AdminResponseViewModel
            {
                Name = dto.Name,
                AdminId = dto.AdminId,
                Email = dto.Email,
                Privileged = dto.Privileged
            };
        }

        public AdminEntity ConvertToAdminEntity(CreateAdminDTO dto)
        {
            return new AdminEntity(dto.Email, dto.Password, dto.Privileged, dto.Name);
        }

        public AdminDTO ConvertToAdminDTO(AdminEntity entity)
        {
            return new AdminDTO()
            {
                AdminId = entity.AdminId,
                Email = entity.Email,
                Name = entity.Name,
                Privileged = entity.Privileged
            };
        }

        public ICollection<AdminDTO> ConvertToDTOs(ICollection<AdminEntity> entities)
        {
            AdminDTO[] dtos = new AdminDTO[entities.Count];

            int i = 0;
            foreach (AdminEntity entity in entities)
            {
                i++;
                dtos[i] = new AdminDTO(entity.AdminId, entity.Email, entity.Name, entity.Privileged);
            }

            return dtos;
        }
    }
}