﻿using System.Collections.Generic;
using TaxModel.EntityDTO.DTOs.Client;
using TaxModel.EntityDTO.Entities;
using TaxModel.EntityDTO.ViewModels.Account;
using TaxModel.EntityDTO.ViewModels.Client;
using TaxModel.Interface.Converters;

namespace TaxModel.Startup.Models.Converters
{
    public class ClientConverter : IClientConverter
    {
        public CreateClientDTO ConvertToCreateClientDTO(CreateClientRequestViewModel vm)
        {
            return new CreateClientDTO
            {
                CompanyName = vm.CompanyName,
                Email = vm.Email,
                Name = vm.Name,
                Password = vm.Password,
                PhoneNumber = vm.PhoneNumber == null ? "" : vm.PhoneNumber
            };
        }

        public ClientEntity ConvertToClientEntity(CreateClientDTO dto)
        {
            return new ClientEntity(dto.Name, dto.Password, dto.Email, dto.PhoneNumber, dto.CompanyName);
        }

        public ClientResponseViewModel ConvertToResponseVm(ClientDTO dto)
        {
            return new ClientResponseViewModel()
            {
                CompanyName = dto.CompanyName,
                Email = dto.Email,
                Name = dto.Name,
                PhoneNumber = dto.PhoneNumber
            };
        }

        public ClientDTO ConvertToClientDTO(ClientEntity entity)
        {
            return new ClientDTO
            {
                ClientId = entity.ClientId,
                Name = entity.Name,
                CompanyName = entity.CompanyName,
                Email = entity.Email,
                PhoneNumber = entity.PhoneNumber
            };
        }

        public ICollection<ClientDTO> ConvertToDTOs(ICollection<ClientEntity> entities)
        {
            ClientDTO[] dtos = new ClientDTO[entities.Count];

            int i = 0;
            foreach (ClientEntity entity in entities)
            {
                dtos[i] = ConvertToClientDTO(entity);
                i++;
            }

            return dtos;
        }

    }
}
