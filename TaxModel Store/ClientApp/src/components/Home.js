import React, { Component } from 'react';
import './Home.css';
import { Link } from 'react-router-dom';

export default class Home extends Component {

  render() {
    return (
      <div>
        <div className="rectangle pb-3">
          <div className="text-wrapper">
            <h1>Discover How Effortless <br />Tax Can Be</h1>
            <h4>Our software solutions are developed by and for tax professionals, in <br /> order to make your tax much faster, easier and efficient.</h4>
          </div>
          <div className="button-wrapper">
            <Link className="btn btn-primary" to="/Services" role="button">Learn more</Link>
          </div>
        </div>
        <div className="triangle-down"></div>
        <div className="home-header">
          <h2>Welcome to the future of tax</h2>
        </div>
        <div className="home-container">
          <div className="home-container-left">
            <p>
              TaxModel is your partner in international tax.
              With our team of professional advisors and
              developers, we are able to combine advisory
              services with innovative technology that deals
              with global tax complexities.<br /><br />
              In order to achieve Tax Advisory 2.0, we
              continuously embed in-house tax expertise into
              software. Each solution we release to the market
              is developed based on the highest technology
              standards and aims to make tax workflows
              transparent and manageable for all companies.
            </p>
          </div>
          <div className="home-container-right">
            <div className="home-sellingpoint">
            <img src="/img/Control.png" />
              <h5>In Control</h5>
              <p>Due diligence ready and in full control of your tax position</p>
            </div>
            <div className="home-sellingpoint">
            <img src="/img/Knowledge.png" />
              <h5>Knowledge</h5>
              <p>Extensive international in-house support experience</p>
            </div>
            <div className="home-sellingpoint">
            <img src="/img/Innovation.png" />
              <h5>Innovation</h5>
              <p>Work with the latest technology in the area of tax</p>
            </div>
            <div className="home-sellingpoint">
            <img src="/img/Time.png" />
              <h5>Time Saving</h5>
              <p>Elimination and standardization of repetitive tax workflows</p>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
