import React, { Component } from 'react';
import FetchUtil from "../../utility/FetchUtil";

import "./EditBusinessPlan.css";

let $ = window.$

class EditBusinessPlan extends Component {

    constructor() {
        super()
        this.state = {
            name: "",
            displayName: "",
            description: "",
            price: 0,
            status: 0,
            message: ""
        };
        this.editBusinessPlanSubmit = this.editBusinessPlanSubmit.bind(this);
        this.archiveBusinessPlanSubmit = this.archiveBusinessPlanSubmit.bind(this);
    }

    componentDidMount() {
        const params = this.props.match.params;


        let fetchOptions = {
            mode: "cors",
            cache: "no-cache",
            credentials: "same-origin"
        };

        FetchUtil.fetchGet(`/api/businessplan/getByName?name=` + encodeURIComponent(params.name), fetchOptions)
            .then((response) => {
                this.setState({
                    name: response.name,
                    displayName: response.displayName,
                    description: response.description,
                    price: response.price,
                    status: response.status == 0
                })
            })
            .catch(function (error) {
                console.error("Failed to get business plan!");
                console.error(error);
            });


    }

    editBusinessPlanSubmit() {

        let succeed = $("#business-plan-edit-succeed");
        let errorElement = $("#business-plan-edit-fail");

        let fetchOptions = {
            mode: "cors",
            cache: "no-cache",
            credentials: "same-origin"
        };

        let postOptions = {
            name: this.state.name,
            displayName: this.state.displayName,
            description: this.state.description,

            price: parseFloat(this.state.price),
            status: this.state.status ? 0 : 1
        };

        FetchUtil.fetchPost("/api/businessplan/edit", fetchOptions, postOptions)
            .then((response) => {

                this.setState({ message: 'Service has been edited' })
                if (succeed.hasClass("d-none")) {
                    succeed.removeClass("d-none");
                    errorElement.addClass("d-none");
                }
                succeed.html("<strong>Business plan edit succeeded.</strong> The business plan has been edited.")

            })
            .catch(function (error) {
                if (errorElement.hasClass("d-none")) {
                    errorElement.removeClass("d-none")
                    succeed.addClass("d-none");
                }
                console.error(error);
                errorElement.html("<strong>Business plan  edit failed</strong> " + error)
            });
    }

    archiveBusinessPlanSubmit() {

        let succeed = $("#business-plan-edit-succeed");
        let errorElement = $("#business-plan-edit-fail");

        let ok = window.confirm('Archive this business plan  ' + this.state.displayName + "?");

        let fetchOptions = {
            mode: "cors",
            cache: "no-cache",
            credentials: "same-origin"
        };

        let postOptions = {
            name: this.state.name,
            displayName: this.state.displayName,
            description: this.state.description,
            price: parseFloat(this.state.price),
            status: 2
        };

        if (ok) {

            FetchUtil.fetchPost("/api/businessplan/update", fetchOptions, postOptions)
                .then((response) => {
                    window.location = "/adminoverview";
                })
                .catch(function (error) {
                    if (errorElement.hasClass("d-none")) {
                        errorElement.removeClass("d-none")
                        succeed.addClass("d-none");
                    }
                    console.error(error);
                    errorElement.html("<strong>Failed to archive business plan</strong> " + error)
                });

            
        }
    }

    render() {
        return (
            <div>
            <div className="bg-light d-flex flex-column p-3">
                <h1 className="d-flex">Edit business plan</h1>
                <input value={this.state.displayName} onChange={(event) => { this.setState({ displayName: event.target.value }) }} type="text" className="form-control mb-3 d-flex" id="edit-businessplan-display-name" aria-describedby="emailHelp" placeholder="Name" />
                <textarea value={this.state.description} onChange={(event) => { this.setState({ description: event.target.value }) }} className="form-control mb-3 d-flex" id="edit-businessplan-description" rows="3" placeholder="Description"></textarea>
                <div className="input-group w-auto mb-3 ml-auto d-flex">
                    <div className="input-group-prepend">
                        <span className="input-group-text">&euro;</span>
                    </div>
                    <input value={this.state.price} onChange={(event) => { this.setState({ price: event.target.value }) }} type="number" className="form-control" placeholder="Price" id="edit-businessplan-price" aria-label="Price" />
                    <div className="input-group-append">
                        <span className="input-group-text">.00</span>
                    </div>
                </div>

                <div className="input-group w-auto ml-auto">
                    <label>
                        <input name="status" checked={this.state.status} onChange={(event) => { this.setState({ status: !this.state.status }) }} type="checkbox" id="0" />  Active for clients <br />
                    </label>
                </div>

                <div className="d-flex flex-row mt-5">
                    <div className="d-flex mr-auto"><button onClick={this.editBusinessPlanSubmit} className="btn btn-primary mx-auto d-flex px-4 py-2">Submit</button></div>
                    <div className="d-flex ml-auto"><button onClick={this.archiveBusinessPlanSubmit } className="btn btn-danger w-auto ml-auto">Archive business plan</button></div>
                </div>
                <input type="text" className="form-control mt-3 d-flex px-4 py-2" id="edit-businessplan-name" defaultValue={this.state.name} readOnly />
            </div>

            <div className="alert alert-success d-none show mx-auto mt-5 px-4 py-2" id="business-plan-edit-succeed" role="alert">
            </div>
            <div className="alert alert-danger d-none show mx-auto mt-3 px-4 py-2" role="alert" id="business-plan-edit-fail" role="alert">
                error
                </div>
            </div>
        );
    }
}

export default EditBusinessPlan;
