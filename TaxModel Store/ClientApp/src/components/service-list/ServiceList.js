import React, { Component } from 'react';
import Service from "../service/Service";
import FetchUtil from "../../utility/FetchUtil";

import "./ServiceList.css";

class ServiceList extends Component {


    state = {
        serviceName: null
    };

    fetchData(value){

    }

    componentDidMount(){
        let fetchOptions = {
            mode: "cors",
            cache: "no-cache",
            credentials: "same-origin"
        };
        
        FetchUtil.fetchGet("/api/Service/GetAllVisibleServices", fetchOptions)
        .then(function(jsonResponse){
            thisComponent.setState({
                Services: jsonResponse.Services
            });
        })
        .catch(function(error){
            console.error(error);
            throw error;
        });
    }
;
    renderService(Service, index){
        return (
            <Service key={index} Service={Service}/>
        );
    }

    renderServices(services){
        if (services){
            let renderedResult = services.map((service, index) => this.renderService(service, index));
            return renderedResult;
        }
    }

    render() {
        return (
            <div>
                {this.renderServices(this.state.Services)}
            </div>
        );
    }
}

export default ServiceList;
