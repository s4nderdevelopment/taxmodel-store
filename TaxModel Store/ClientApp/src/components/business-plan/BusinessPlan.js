import React, { Component } from 'react';

import "./BusinessPlan.css";

class BusinessPlan extends Component {

    state = {
        name: null
    };

    fetchData(value){

    }

    componentDidMount(){
        if(!this.props.businessPlan){
            throw new Error("Could not render: Business plan was not set!");
        }

        this.setState({
            businessPlan: this.props.businessPlan
        });
    }
    
    render() {
        if(!this.state.businessPlan){
            return (
                <div>

                </div>
            )
        }
        return (
            <div>
                <h4>Name: {this.state.businessPlan.displayName}</h4>
                <p>Description: {this.state.businessPlan.description}</p>
                <p>Price: €{parseFloat(this.state.businessPlan.price).toFixed(2).toString()}</p>
            </div>
        );
    }
}

export default BusinessPlan;
