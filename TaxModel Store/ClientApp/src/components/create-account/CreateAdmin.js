import React, { Component } from 'react';
import FetchUtil from "../../utility/FetchUtil";


const $ = window.$
class CreateAdmin extends Component {

    constructor(props) {
        super(props);
    }   

    onCreateAdmin = () => {
        const fetchOptions = {
            mode: "cors",
            cache: "no-cache",
            credentials: "same-origin"
        };

        let password = document.getElementById("CreationPassword").value;
        let passwordConfirm = document.getElementById("CreationPasswordConfirm").value;

        const succeed = $("#admin-create-succeed");
        const errorElement = $("#admin-create-fail");

        if(password !== passwordConfirm){
            if (errorElement.hasClass("d-none")) {
                errorElement.removeClass("d-none")
                succeed.addClass("d-none");
            }
           errorElement.html("<strong>Admin creation failed</strong> passwords do not match");
            return;
        }

        let postBody = {
            name: document.getElementById("CreationName").value,
            email: document.getElementById("CreationEmail").value,
            password: password,
            privileged: document.getElementById("CreationPrivileged").checked
        };

        FetchUtil.fetchPost("/api/Admin/create", fetchOptions, postBody)
            .then((response) => {
                if (response) {
                    this.setState({ message: 'New admin has been created' })
                    console.log(response);
                }
                if (succeed.hasClass("d-none")) {
                    succeed.removeClass("d-none");
                    errorElement.addClass("d-none");
                }               
            })
            .catch(function (error) {
                if (errorElement.hasClass("d-none")) {
                    errorElement.removeClass("d-none")
                    succeed.addClass("d-none");
                }
               errorElement.html("<strong>Admin creation failed</strong> " + error)
            });
    }

    render() {
        return (
            <div className="bg-light d-flex flex-column p-3">
                <h1 className="d-flex flex-row">Create admin</h1>
                    <input ref="name" type="text" className="form-control mb-3 d-flex" id="CreationName" placeholder="Name" />
                    <input type="email" ref="email" className="form-control mb-3 d-flex" id="CreationEmail" placeholder="Email address"/>
                    <input type="password" ref="password" className="form-control mb-3 d-flex" id="CreationPassword" placeholder="Password"/>
                    <input type="password" ref="password-confirm" className="form-control mb-3 d-flex" id="CreationPasswordConfirm" placeholder="Confirm password"/>
                    <label>Privileged? </label>
                    <input type="checkbox" ref="privileged" className="form-control h-auto mr-auto w-auto d-flex" id="CreationPrivileged"/>
                <br></br>
                <button onClick={this.onCreateAdmin} className="btn btn-primary mr-auto d-flex px-4 py-2">Register</button>
                <div className="alert alert-success fade show d-none" id="admin-create-succeed" role="alert">
                    <strong>Admin creation succeeded</strong> the admin has been created.

                    </div>
                <div className="alert alert-danger fade show d-none" role="alert" id="admin-create-fail" role="alert">
                    error
                </div>
            </div>
        );
    }
}

export default CreateAdmin
