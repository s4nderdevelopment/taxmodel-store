import React, { Component } from 'react';
import FetchUtil from "../../utility/FetchUtil";


const $ = window.$
class CreateClient extends Component {

    constructor(props) {
        super(props);
    }

    onCreateClient = () => {
        const fetchOptions = {
            mode: "cors",
            cache: "no-cache",
            credentials: "same-origin"
        };

        let postBody =
        {
            name: this.refs.name.value,
            email: this.refs.email.value,
            password: this.refs.password.value,
            phoneNumber: this.refs.phoneNumber.value,
            companyName: this.refs.companyName.value
        };

        const succeed = $("#account-create-succeed");
        const errorElement = $("#account-create-fail");

        FetchUtil.fetchPost("/api/Client/create", fetchOptions, postBody)
            .then((response) => {
                if (response) {
                    this.setState({ message: 'New account has been created' })
                    console.log(response);
                }
                if (succeed.hasClass("d-none")) {
                    succeed.removeClass("d-none");
                    errorElement.addClass("d-none");
                }
            })
            .catch(function (error) {
                if (errorElement.hasClass("d-none")) {
                    errorElement.removeClass("d-none")
                    succeed.addClass("d-none");
                }
                errorElement.html("<strong>Account creation failed</strong> " + error)
            });
    }

    render() {
        return (
            <div className="bg-light d-flex flex-column p-3">
                <h1 className="d-flex">Register</h1>
                <input ref="name" type="text" className="form-control mb-3 d-flex" id="CreationName" placeholder="Name" />
                <input type="email" ref="email" className="form-control mb-3 d-flex" id="CreationEmail" placeholder="Email address" />
                <input type="password" ref="password" className="form-control mb-3 d-flex" id="CreationPassword" placeholder="Password" />
                <input type="password" ref="password" className="form-control mb-3 d-flex" id="CreationPasswordConfirm" placeholder="Confirm password" />
                <input type="text" ref="phoneNumber" className="form-control mb-3 d-flex" id="CreationPhoneNUmber" placeholder="Phone Number" />
                <input type="text" ref="companyName" className="form-control mb-3 d-flex" id="CreationCompanyName" placeholder="Company Name" />
                <br></br>
                <button onClick={this.onCreateClient} className="btn btn-primary mr-auto d-flex px-4 py-2">Register</button>
                <div className="alert alert-success fade show d-none" id="account-create-succeed" role="alert">
                    <strong>Registration succeeded</strong> your account has been created.

                    </div>
                <div className="alert alert-danger fade show d-none" role="alert" id="account-create-fail" role="alert">
                    error
                </div>
            </div>
        );
    }
}

export default CreateClient
