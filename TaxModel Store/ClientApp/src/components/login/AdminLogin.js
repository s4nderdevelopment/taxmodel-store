import React, { Component } from 'react';
import FetchUtil from "../../utility/FetchUtil";


const $ = window.$
class AdminLogin extends Component {

    constructor(props) {
        super(props);
    }

    onAdminLogin = () => {
        const fetchOptions = {
            mode: "cors",
            cache: "no-cache",
            credentials: "same-origin"
        };

        let postBody =
        {
            email: this.refs.email.value,
            password: this.refs.password.value
        };

        const succeed = $("#account-login-succeed");
        const errorElement = $("#account-login-fail");

        FetchUtil.fetchPost("/api/Admin/login", fetchOptions, postBody)
            .then((response) => {
                if (response) { 
                    localStorage.setItem('token', response.token)
                    window.location = "/AdminOverview"
                }
                if (succeed.hasClass("d-none")) {
                    succeed.removeClass("d-none");
                    errorElement.addClass("d-none");
                }
            })
            .catch(function (error) {
                if (errorElement.hasClass("d-none")) {
                    errorElement.removeClass("d-none")
                    succeed.addClass("d-none");
                }
                errorElement.html("<strong>Account login failed</strong> " + error)
            });
    }

    render() {
        return (
            <div className="bg-light d-flex flex-column p-3">
                <h1 className="d-flex">Admin Login</h1>
                <input type="email" ref="email" className="form-control mb-3 d-flex" id="LoginEmail" placeholder="Email address" />
                <input type="password" ref="password" className="form-control mb-3 d-flex" id="LoginPassword" placeholder="Password" autoComplete="off" />
                <br></br>
                <button onClick={this.onAdminLogin} className="btn btn-primary mr-auto d-flex px-4 py-2">Login</button>
                <div className="alert alert-success fade show d-none" id="account-login-succeed" role="alert">
                    <strong>Logged in successfully</strong> you have been logged in.

                    </div>
                <div className="alert alert-danger fade show d-none" role="alert" id="account-login-fail" role="alert">
                    error
                </div>
            </div>
        );
    }
}

export default AdminLogin
