import React, { Component } from 'react';
import FetchUtil from "../../utility/FetchUtil";

import "./AdminOverview.css";

let $ = window.$;

class AdminOverview extends Component {

    state = {
        services: {

        }
    }

    foldButtonClick(serviceName){
        this.state.services[serviceName].showBusinessPlans = !this.state.services[serviceName].showBusinessPlans;
        let foldButton = $(".btn-fold.btn-fold-" + serviceName);
        if(this.state.services[serviceName].showBusinessPlans){
            foldButton.addClass("btn-fold-open");
        }else{
            foldButton.removeClass("btn-fold-open");
        }
        this.setState(this.state);
    }

    componentDidMount(){
        let fetchOptions = {
            mode: "cors",
            cache: "no-cache",
            credentials: "same-origin"
        };

        FetchUtil.fetchGet("/api/service/getAllVisibleToAdminServices", fetchOptions)
        .then(async (response) => {
            let newServices = {};
            for(let i = 0; i < response.length; i++){
                let service = response[i];
                if(service.status != 2){
                    service.businessPlans = {};
                    service.showBusinessPlans = false;
                    let businessPlanResponse = await FetchUtil.fetchGet("/api/businessPlan/getByService?serviceName=" + encodeURIComponent(service.name), fetchOptions)
                    .catch((error) => {
                        console.error(error);
                    });
                    businessPlanResponse.forEach((businessPlan) => {
                        service.businessPlans[businessPlan.name] = businessPlan;
                    });
                    newServices[service.name] = service;
                }
            };
            this.setState({
                services: newServices
            });
        })
        .catch(function(error){
            console.error("Failed to create business plan!");
            console.error(error);
        });
    }

    toggleServiceVisibility(service){
        let fetchOptions = {
            mode: "cors",
            cache: "no-cache",
            credentials: "same-origin"
        };

        let postParams = {
            name: service.name,
            displayName: service.displayName,
            description: service.description,
            status: 1 - service.status
        };

        FetchUtil.fetchPost("/api/service/update", fetchOptions, postParams)
        .then((response) => {
            let newServices = {...this.state.services};
            newServices[service.name].status = 1 - service.status;
            this.setState({
                services: newServices
            });
        })
        .catch((error) => {
            console.error(error);
        });
    }

    editBusinessPlan(businessPlan){
        window.location = "/EditBusinessPlan/" + encodeURIComponent(businessPlan.name);
    }

    editService(service){
        window.location = "/EditService/" + encodeURIComponent(service.name);
    }

    toggleBusinessPlanVisibility(service, businessPlan){
        let fetchOptions = {
            mode: "cors",
            cache: "no-cache",
            credentials: "same-origin"
        };

        let postParams = {
            name: businessPlan.name,
            displayName: businessPlan.displayName,
            description: businessPlan.description,
            price: parseFloat(businessPlan.price),
            status: 1 - businessPlan.status
        };

        FetchUtil.fetchPost("/api/businessPlan/edit", fetchOptions, postParams)
        .then((response) => {
            let newServices = {...this.state.services};
            newServices[service.name].businessPlans[businessPlan.name].status = 1 - businessPlan.status;
            this.setState({
                services: newServices
            });
        })
        .catch((error) => {
            console.error(error);
        });
    }

    renderBusinessPlans(service){
        let businessPlanElements = [];

        let businessPlanNames = Object.keys(service.businessPlans);

        businessPlanNames.forEach((businessPlanName, i) => {
            if(service.showBusinessPlans){
                let businessPlan = service.businessPlans[businessPlanName];
                let description = businessPlan.description;
                const maxLength = 32;
                if(description.length > maxLength + 3){
                    description = description.substring(0, maxLength) + "...";
                }

                let bgClass = "bg-light";

                if(businessPlan.status === 2){
                    return;
                }

                businessPlanElements.push(
                    <div key={i} className="col col-12 d-flex px-0">
                        <div className={"ml-5 w-auto col d-flex rounded p-3 mt-3 " + bgClass}>
                            <div className="col col-3 d-flex px-0">
                                <p className="my-auto">{businessPlan.displayName}</p>
                            </div>
                            <div className="col col-6 d-flex px-0">
                                <p className="my-auto">{description}</p>
                            </div>
                            <div className="col col-3 d-flex px-0 flex-row">
                                <button key={businessPlan.status} className="btn d-flex ml-auto my-auto mr-2" onClick={() => {this.toggleBusinessPlanVisibility(service, businessPlan)}}>
                                    {businessPlan.status == 1 ? (<i className={"fas fa-eye-slash"}></i>) : (<i className={"fas fa-eye"}></i>)}
                                </button>
                                <button className="ml-2 my-auto d-flex btn btn-primary" onClick={() => {this.editBusinessPlan(businessPlan)}}>
                                    Edit
                                </button>
                            </div>
                        </div>
                    </div>
                );
            }
        });

        return businessPlanElements;
    }

    renderServices(){
        let serviceElements = [];

        let serviceNames = Object.keys(this.state.services);

        serviceNames.forEach((serviceName, i) => {
            let service = this.state.services[serviceName];
            let description = service.description;
            const maxLength = 32;
            if(description.length > maxLength + 3){
                description = description.substring(0, maxLength) + "...";
            }

            let marginBottom = "mb-3 ";

            if(i == serviceNames.length - 1){
                marginBottom = "";
            }

            let index = i + 0;
            
            if(service.status === 2){
                return;
            }

            let hasSlashClass = "";

            if(service.status == 1){
                hasSlashClass = "-slash";
            }

            serviceElements.push(
                <div key={index} className={"d-flex p-3 " + marginBottom + "rounded w-100 bg-gray"}>
                    <div className="d-flex flex-row w-100 flex-wrap">
                        <div className="col col-3 d-flex px-0">
                            <button key={service.showBusinessPlans} className={"btn btn-outline-secondary text-body p-1 mr-2 my-auto d-inline-block btn-fold btn-fold-" + index + (service.showBusinessPlans ? " btn-fold-open" : "")} onClick={() => {this.foldButtonClick(service.name)}}>
                                <div className="btn-fold-icon" style={{width: "20px", height: "20px"}}>
                                    <i className="fas fa-caret-up" style={{"marginBottom": "3px"}}></i>
                                </div>
                            </button>
                            <p className="my-auto">{service.displayName}</p>
                        </div>
                        <div className="col col-6 d-flex px-0">
                            <p className="my-auto">{description}</p>
                        </div>
                        <div className="col col-3 d-flex px-0 flex-row">
                            <button key={service.status} className="btn d-flex ml-auto my-auto mr-2" onClick={() => {this.toggleServiceVisibility(service)}}>
                                {service.status == 1 ? (<i className={"fas fa-eye-slash"}></i>) : (<i className={"fas fa-eye"}></i>)}
                            </button>
                            <button className="ml-2 mr-3 d-flex btn my-auto btn-primary" onClick={() => {this.editService(service)}}>
                                Edit
                            </button>
                        </div>
                        {this.renderBusinessPlans(service)}
                    </div>
                </div>
            );
        });

        return serviceElements;
    }

    render() {
        return (
            <div className="bg-white d-flex flex-column p-3">
                <h1 className="d-flex">Admin overview</h1>
                <div className="service-list bg-white rounded p-4">
                    <div className="service-list bg-white rounded d-flex flex-column p-3">
                        {this.renderServices()}
                    </div>
                </div>
            </div>
        );
    }
}

export default AdminOverview;
