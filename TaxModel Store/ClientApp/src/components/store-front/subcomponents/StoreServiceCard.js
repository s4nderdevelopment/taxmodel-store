import React, { Component } from "react";
import "../StoreFront.css";

export class ServiceCard extends Component {
state={};

  componentWillMount() {
   this.setState({
     serviceInfo: this.props.item
   });
  }

  render() {
    return (
      <div className="d-flex flex-column align-items-center justify-content-around mr-3 ml-3 mb-3 mt-3 serviceCard">
        <div className="mb-2 title">{this.state.serviceInfo.displayName}</div>
        <div className="">
          <div>{this.state.serviceInfo.isActivated ? "True":"False"}</div>
        </div>
        <div className="mb-2">{this.state.serviceInfo.description}</div>
        <div className="d-inline-flex flex-column align-items-center">
          <a href="/BusinessPlan" className="btn btn-primary mb-1">
            Business Plans
          </a>
          <a href="#" className="btn btn-success">
            Demo
          </a>
        </div>
      </div>
    );
  }
}
export default ServiceCard;
