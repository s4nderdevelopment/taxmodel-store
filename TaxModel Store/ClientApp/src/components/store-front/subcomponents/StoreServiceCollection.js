import React, { Component } from "react";
import ServiceCard from "../subcomponents/StoreServiceCard";
import "../StoreFront.css";
import Tabs from './Tabs';
import FetchUtil from "../../../utility/FetchUtil";

class ServiceCollection extends Component {
  state = {};
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: []
    };
  }

  componentDidMount() {
    let fetchOptions = {
        mode: "cors",
        cache: "no-cache",
        credentials: "same-origin"
    };

    FetchUtil.fetchGet("/api/service/GetAllVisibleServices", fetchOptions)
    .then(
      (jsonResponse) => {
        this.setState({
          isLoaded: true,
          items: jsonResponse
        });
      },
      (error) => {
        this.setState({
          isLoaded: true,
          error
        });
      }
    );
  }

  renderServiceCard(item, index) {
    return <ServiceCard key={index} item={item} />;
  }

  renderServiceCards(items) {
    let renderResult = items.map((item, index) => this.renderServiceCard(item, index));
    return renderResult;
  }

  render() {
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    }
    else if (!isLoaded) {
      return <div>Loading...</div>;
    }
    else {
      return (
        <div className="d-flex serviceCollection justify-content-center align-content-center flex-wrap">
          <Tabs>
            <div label="Tab1">
              {this.renderServiceCards(items.slice(0, 3))}
            </div>
            <div label="Tab2">
              {this.renderServiceCards(items.slice(3, 5))}
            </div>
          </Tabs>
          {/*this.renderServiceCards(this.state.items)*/}
        </div>
      );
    }
  }
}
export default ServiceCollection;
