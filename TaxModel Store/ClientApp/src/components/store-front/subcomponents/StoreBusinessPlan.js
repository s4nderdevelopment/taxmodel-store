import React, { Component } from "react";
import "../StoreFront.css";

export class BusinessPlan extends Component {

    render() {
        return (
            <div className="card text-center">
                <div className="card text-center">
                    <div className="card-header">
                        BusinessPlan_Name
                    </div>
                    <div className="card-body">
                        <p className="card-text">BusinessPlan_Description</p>
                    </div>
                </div>
            </div>
        );
    }
}
export default BusinessPlan;
