import React, { Component } from "react";
import "../StoreFront.css";

export class PaymentPlan extends Component {

    render() {
        return (
            <div className="row">
                <div className="card text-center normalcard">
                    <div className="card-header normalchoice">
                        Normal
                    </div>
                    <div className="card-body normalchoiceCard">
                        <h3 className="card-title">1 year</h3>
                        <p className="card-text">PaymentPlan_Description </p>
                        <a href="#" className="btn btn-primary">Select Plan</a>
                    </div>
                </div>
                <div className="card text-center">
                    <div className="card-header bestchoice">
                        Recommended
                    </div>
                    <div className="card-body bestchoiceCard">
                        <h3 className="card-title">3 years</h3>
                        <p className="card-text">PaymentPlan_Description</p>
                        <a href="#" className="btn btn-primary">Select Plan</a>
                    </div>
                </div>
            </div>
        );
    }
}
export default PaymentPlan;
