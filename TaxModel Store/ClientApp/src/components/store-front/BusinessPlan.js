import React, { Component } from "react";
import StoreBusinessPlan from "./subcomponents/StoreBusinessPlan";
import StorePaymentPlan from "./subcomponents/StorePaymentPlan";

export default class BusinessPlan extends Component {
    
  render() {
    return (
      <div className="col-md-12 d-flex flex-column align-items-center">
          <h1>Business Plan</h1>
          <StoreBusinessPlan />
          <h1>Payment Plans</h1>
          <StorePaymentPlan />
      </div>
    );
  }
}
