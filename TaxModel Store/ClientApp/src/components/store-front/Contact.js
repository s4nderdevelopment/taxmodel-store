import React, { Component } from 'react';

export default class Contact extends Component {
    componentDidMount() {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        let forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        let validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }
    render() {
        return (
            <div>
                <div className="contact-container">
                    <div className="contact-container-left">
                        <div className="container-adress">
                            <h2>Taxmodel</h2>
                            <p>
                                <a href="https://www.google.nl/maps/place/Tax+Model+B.V./@51.690204,5.2932503,17z/data=!3m1!4b1!4m5!3m4!1s0x47c6ee8ac7556207:0xcf9d55cf5b28ad2f!8m2!3d51.690204!4d5.295439">Stationsplein 12<br />
                                5211 AP<br />
                                ‘s-Hertogenbosch<br />
                                The Netherlands
                            </a>
                            </p>
                        </div>
                        <div className="container-email">
                            <p>
                                <a href="mailto:info@tax-model.com">info@tax-model.com</a>
                                <br />
                                <a href="tel:+31738000030">+ 31 (0)73 800 00 30</a>
                            </p>
                        </div>
                    </div>
                    <div className="contact-container-right">
                        <h2>Contact us</h2>
                        <form className="needs-validation" noValidate>
                            <div className="form-row">
                                <div className="col-md-6 mb-3">
                                    <label htmlFor="validationCustom01">First name</label>
                                    <span>*</span>
                                    <input type="text" className="form-control" id="validationCustom01" required></input>
                                    <div className="invalid-feedback">
                                        Please fill in a valid name.
                                    </div>
                                </div>
                                <div className="col-md-6 mb-3">
                                    <label htmlFor="validationCustom02">Last name</label>
                                    <span>*</span>
                                    <input type="text" className="form-control" id="validationCustom02" required></input>
                                    <div className="invalid-feedback">
                                        Please fill in a valid last name.
                                    </div>
                                </div>
                                <div className="col-md-6 mb-3">
                                    <label htmlFor="validationCustom03">Email address</label>
                                    <span>*</span>
                                    <input type="email" className="form-control" id="validationCustom03" placeholder="name@example.com" required></input>
                                    <div className="invalid-feedback">
                                        Please provide a valid email address.
                                    </div>
                                </div>
                                <div className="col-md-6 mb-3">
                                    <label htmlFor="validationCustom04">Mobile phone number</label>
                                    <span>*</span>
                                    <input type="text" className="form-control" id="validationCustom04" required></input>
                                    <div className="invalid-feedback">
                                        Please provide a valid phone number.
                                    </div>
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="col-md-12 mb-3">
                                    <label htmlFor="validationCustom05">Company name</label>
                                    <span>*</span>
                                    <input type="text" className="form-control" id="validationCustom05" required></input>
                                    <div className="invalid-feedback">
                                        Please complete this required field.
                                    </div>
                                </div>
                            </div>
                            <div className="form-row">
                                <div className="col-md-12 mb-3">
                                    <label htmlFor="validationCustom05">Message</label>
                                    <span>*</span>
                                    <textarea className="form-control message-area" type="text" required></textarea>
                                </div>
                            </div>
                            <button className="btn btn-primary" type="submit">Submit</button>
                        </form>
                    </div>
                </div>

            </div>
        );
    }
}