import React, { Component } from "react";
import StoreServiceCollection from "./subcomponents/StoreServiceCollection";

export default class Solutions extends Component {
  render() {
    return (
      <div className="col-md-12 d-flex flex-column align-items-center">
        <div className="d-flex justify-content-center pb-3">
          <h1>Our Services</h1>
        </div>
        <StoreServiceCollection />
      </div>
    );
  }
}
