﻿import React, { Component } from 'react';
import FetchUtil from "../../utility/FetchUtil";

import "./EditService.css";

let $ = window.$


class EditService extends Component {

    componentDidMount() {
        const params = this.props.match.params;

        this.ServiceGetByName(params.name);
    }

    ServiceGetByName(getName) {

        let succeed = $("#service-edit-succeed");
        let errorElement = $("#service-edit-fail");

        let fetchOptions = {
            mode: "cors",
            cache: "no-cache",
            credentials: "same-origin"
        };
        FetchUtil.fetchGet("/api/service/getByName?name=" + encodeURIComponent(getName), fetchOptions, {})
        .then((response) => {
            if (response) {
                this.setState({
                    service: {...response},
                    message: 'Service has been fetched'
                });
                $("#edit-service-display-name").val(response.displayName);
                $("#edit-service-description").val(response.description);
                $("#edit-service-name").val(response.name);
                if (succeed.hasClass("d-none")) {
                    succeed.removeClass("d-none");
                    errorElement.addClass("d-none");
                    succeed.html("<strong>Service load succeeded</strong> service has been fetched.")
                }
            }

        })
        .catch(function (error) {
            if (errorElement.hasClass("d-none")) {
                errorElement.removeClass("d-none")
                succeed.addClass("d-none");
            }

            errorElement.html("<strong>Service failed to load</strong> " + error)
        });
    }

    state = {
        service: {
            displayName: "",
            name: "",
            description: ""
        }
    }

    ArchiveButtonClick() {
        let editeddisplayname = document.getElementById("edit-service-display-name");
        let ok = window.confirm('Archive this service ' + editeddisplayname.value + "?");
        let succeed = $("#service-edit-succeed");
        let errorElement = $("#service-edit-fail");

        if (ok) {

            let fetchOptions = {
                mode: "cors",
                cache: "no-cache",
                credentials: "same-origin"
            };

            let displayNameElement = document.getElementById("edit-service-display-name");
            let displayName = displayNameElement.value;
    
            let descriptionElement = document.getElementById("edit-service-description");
            let description = descriptionElement.value;
    
            let nameElement = document.getElementById("edit-service-name");
            let name = nameElement.value;

            let succeed = $("#service-edit-succeed");
            let errorElement = $("#service-edit-fail");

            FetchUtil.fetchPost("/api/service/archive?name=" + encodeURIComponent(this.state.service.name), fetchOptions, {})
                .then((response) => {            
                        window.location = "/adminoverview";
                })
                .catch((error) => {
                    if (errorElement.hasClass("d-none")) {
                        errorElement.removeClass("d-none")
                        succeed.addClass("d-none");
                    }
                    console.error(error);
                    errorElement.html("<strong>Service archive failed</strong> " + error)
                });

        } else {



        }
    }
    EditServiceSubmit() {
        let fetchOptions = {
            mode: "cors",
            cache: "no-cache",
            credentials: "same-origin"
        };
        let displayNameElement = document.getElementById("edit-service-display-name");
        let displayName = displayNameElement.value;

        let descriptionElement = document.getElementById("edit-service-description");
        let description = descriptionElement.value;

        let nameElement = document.getElementById("edit-service-name");
        let name = nameElement.value;

        let succeed = $("#service-edit-succeed");
        let errorElement = $("#service-edit-fail");

        let postBody = {
            name: name,
            displayName: displayName,
            description: description
        };

        FetchUtil.fetchPost("/api/service/update", fetchOptions, postBody)

            .then((response) => {
                if (response) {

                    this.setState({ message: 'Service has been edited' })
                    if (succeed.hasClass("d-none")) {
                        succeed.removeClass("d-none");
                        errorElement.addClass("d-none");
                    }
                    succeed.html("<strong>Service edit succeeded</strong> the service has been edited.")
                }
            })
            .catch(function (error) {
                if (errorElement.hasClass("d-none")) {
                    errorElement.removeClass("d-none")
                    succeed.addClass("d-none");
                }
                console.error(error);
                errorElement.html("<strong>Service edit failed</strong> " + error)
            });
    }

    onDisplayNameChange(event){
        this.state.service.displayName = event.target.value;
    }

    render() {
        return (
            <div>
                <div>
                    <div className="bg-light d-flex flex-column p-3">
                        <h1 className="d-flex">Edit service</h1>
                        <input type="text" className="form-control mb-3 " id="edit-service-display-name" defaultValue={this.state.service.displayName} />
                        <textarea className="form-control mb-3 " id="edit-service-description" rows="3" defaultValue={this.state.service.description}></textarea>
                        <div className="d-flex flex-row">
                        <button className="btn btn-primary d-flex mr-auto px-4 py-2" onClick={() => {this.EditServiceSubmit()}}>Submit</button>
                        <button className="btn btn-danger d-flex ml-auto px-4 py-2" onClick={() => {this.ArchiveButtonClick()}} >Archive service</button>
                        </div>
                       
                        <input type="text" className="form-control mt-3 px-4 py-2" id="edit-service-name" defaultValue={this.state.service.name} readOnly />
                    </div>
                
                </div>
                <div className="alert alert-success d-none show mx-auto mt-5 px-4 py-2" id="service-edit-succeed" role="alert">
                </div>
                <div className="alert alert-danger d-none show mx-auto mt-3 px-4 py-2" role="alert" id="service-edit-fail" role="alert">
                    error
                </div>
            </div>
        );
    }
}

export default EditService;
