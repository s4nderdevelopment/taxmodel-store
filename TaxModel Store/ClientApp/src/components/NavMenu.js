import React, { Component } from 'react';
import './NavMenu.css';
import { Link } from "react-router-dom";

export class NavMenu extends Component {
  static displayName = NavMenu.name;

  constructor (props) {
    super(props);

    this.toggleNavbar = this.toggleNavbar.bind(this);
    this.state = {
      collapsed: true
    };
  }

  toggleNavbar () {
    this.setState({
      collapsed: !this.state.collapsed
    });
  }

  render () {
    return (
      <header className="bg-white">
        <nav className="navbar navbar-expand-lg navbar-light navbar-expand-sm navbar-toggleable-sm ng-white border-bottom mb-3">
          <div className="container">
            <Link className="navbar-brand mr-2" to="/"><img width="200" src="https://tax-model.com/wp-content/uploads/TaxModel_Logo_Color_2021.svg" /></Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>

            <div className="collapse navbar-collapse" id="navbarColor01">
              <ul className="navbar-nav ml-auto">
                <li className="nav-item active">
                  <Link className="nav-link" to="/">Home</Link>
                </li>
                <li className="nav-item dropdown">
                  <Link className="nav-link dropdown-toggle" data-toggle="dropdown" to="#" role="button" aria-haspopup="true" aria-expanded="false">Services</Link>
                  <div className="dropdown-menu">
                    <Link className="dropdown-item" to="/Services">All services</Link>
                    <div className="dropdown-divider"></div>
                    <Link className="dropdown-item" to="#">Service 1</Link>
                    <Link className="dropdown-item" to="#">Service 2</Link>
                    <Link className="dropdown-item" to="#">Service 3</Link>
                  </div>
                </li>
                {/* <li className="nav-item">
                  <Link className="nav-link" to="#" onClick={function(){window.location = "https://tax-model.com/about"}}>About</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="#" onClick={function(){window.location = "https://tax-model.com/news"}}>News</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="#" onClick={function(){window.location = "https://tax-model.com/careers"}}>Careers</Link>
                </li> */}
                <li className="nav-item">
                  <Link className="nav-link" to="/Contact">Contact</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/CreateService">(Service creation</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/CreateBusinessPlan">Business plan creation</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/AdminOverview">Admin overview</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/CreateAdmin">New Admin)</Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
      </header>
    );
  }
}
