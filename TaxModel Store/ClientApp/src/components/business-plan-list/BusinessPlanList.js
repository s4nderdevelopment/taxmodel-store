import React, { Component } from 'react';
import BusinessPlan from "../business-plan/BusinessPlan";
import FetchUtil from "../../utility/FetchUtil";

import "./BusinessPlanList.css";

class BusinessPlanList extends Component {

    state = {
        serviceName: null
    };

    componentDidMount(){
        let thisComponent = this;
        if(!this.props.serviceName){
            throw new Error("Could not fetch: Service name was not set!");
        }
        let fetchOptions = {
            mode: "cors",
            cache: "no-cache",
            credentials: "same-origin"
        };
        
        FetchUtil.fetchGet("/api/businessplan/byServiceName?serviceName=" + encodeURIComponent(thisComponent.props.serviceName), fetchOptions)
        .then(function(jsonResponse){
            thisComponent.setState({
                serviceName: thisComponent.props.serviceName,
                businessPlans: jsonResponse.businessPlans
            });
        })
        .catch(function(error){
            console.error(error);
            /*
                Mock/template data is inserted here. Remove in production!
            */
            if(true){
                thisComponent.setState({
                    serviceName: thisComponent.props.serviceName,
                    businessPlans: [
                        {
                            name: "tp-benchmark",
                            displayName: "TP Benchmark",
                            description: "This is the description of the TP Benchmark...",
                            price: 1000
                        },
                        {
                            name: "tp-cbc",
                            displayName: "TP CBC",
                            description: "This is the description of the TP CBC...",
                            price: 2000
                        },
                        {
                            name: "tp-doc",
                            displayName: "TP Doc",
                            description: "This is the description of the TP Doc...",
                            price: 3000
                        }
                    ]
                });
                return;
            }
            /*
                End of mock/template data...
            */
            throw error;
        });
    }
;
    renderBusinessPlan(businessPlan, index){
        return (
            <BusinessPlan key={index} businessPlan={businessPlan}/>
        );
    }

    renderBusinessPlans(businessPlans){
        if(businessPlans){
            let renderedResult = businessPlans.map((businessPlan, index) => this.renderBusinessPlan(businessPlan, index));
            return renderedResult;
        }
    }

    render() {
        return (
            <div>
                {this.renderBusinessPlans(this.state.businessPlans)}
            </div>
        );
    }
}

export default BusinessPlanList;
