import React, { Component } from 'react';
import BusinessPlanList from "../business-plan-list/BusinessPlanList";
import FetchUtil from "../../utility/FetchUtil";

import "./CreateBusinessPlan.css";

let $ = window.$;

class CreateBusinessPlan extends Component {

    createBusinessPlanSubmit(){
        let fetchOptions = {
            mode: "cors",
            cache: "no-cache",
            credentials: "same-origin"
        };

        let displayNameElement = document.getElementById("create-businessplan-display-name");
        let displayName = displayNameElement.value;

        let descriptionElement = document.getElementById("create-businessplan-description");
        let description = descriptionElement.value;

        let priceElement = document.getElementById("create-businessplan-price");
        let price = priceElement.value;

        let postBody = {
            displayName: displayName,
            description: description,
            price: parseFloat(price)
        };

        let succeed = $("#business-plan-create-succeed");
        let errorElement = $("#business-plan-create-fail");

        FetchUtil.fetchPost("/api/businessplan/create", fetchOptions, postBody)
        .then(function(response){
                if (succeed.hasClass("d-none")) {
                    succeed.removeClass("d-none");
                    errorElement.addClass("d-none");
                }
        })
        .catch(function(error){
                if (errorElement.hasClass("d-none")) {
                    errorElement.removeClass("d-none")
                    succeed.addClass("d-none");
                }
               errorElement.html("<strong>Business plan creation failed</strong> " + error)
            console.error("Failed to create business plan!");
            console.error(error);
        });
    }

    render() {
        return (
            <div className="bg-light d-flex flex-column p-3">
                <h1 className="d-flex">Create business plan</h1>
                <input type="text" className="form-control mb-3 d-flex" id="create-businessplan-display-name" aria-describedby="displayName" placeholder="Name" />
                <textarea className="form-control mb-3 d-flex" id="create-businessplan-description" rows="3" placeholder="Description"></textarea>
                <div className="input-group w-auto mb-3 mr-auto d-flex">
                    <div className="input-group-prepend">
                        <span className="input-group-text">&euro;</span>
                    </div>
                    <input type="text" className="form-control" placeholder="Price" id="create-businessplan-price" aria-label="Price" />
                    <div className="input-group-append">
                        <span className="input-group-text">.00</span>
                    </div>
                </div>
                <button className="btn btn-primary mr-auto d-flex px-4 py-2" onClick={this.createBusinessPlanSubmit}>Submit</button>
                <div className="alert alert-success fade show d-none" id="business-plan-create-succeed" role="alert">
                    <strong>Business plan creation succeeded</strong> the business plan has been created.

                    </div>
                <div className="alert alert-danger fade show d-none" role="alert" id="business-plan-create-fail" role="alert">
                    error
                </div>
            </div>
        );
    }
}

export default CreateBusinessPlan;
