import React, { Component } from 'react';
import FetchUtil from "../../utility/FetchUtil";


let $ = window.$
class CreateService extends Component {
    static displayName = CreateService.name;

    constructor(props) {
        super(props);
        this.state = {
            message: ''
        };
    }
    onCreateService = () => {
        let fetchOptions = {
            mode: "cors",
            cache: "no-cache",
            credentials: "same-origin"
        };

        let postBody =
        {
            description: this.refs.description.value,
            displayName: this.refs.displayName.value
        };

        let errorElement = $("#service-create-fail");
        let succeed = $("#service-create-succeed");

        FetchUtil.fetchPost("/api/Service/create", fetchOptions, postBody)
            .then((response) => {
                if (response) {
                    this.setState({ message: 'New service has been created' })
                }
                if (succeed.hasClass("d-none")) {
                    succeed.removeClass("d-none");
                    errorElement.addClass("d-none");
                }
            })
            .catch(function (error) {
                if (errorElement.hasClass("d-none")) {
                    errorElement.removeClass("d-none")
                    succeed.addClass("d-none");
                }
               errorElement.html("<strong>Service creation failed</strong> " + error)
            });
    }
    render() {
        return (
            <div className="bg-light d-flex flex-column p-3">
                <h1 className="d-flex" >Create a service</h1>
                    <input ref="displayName" type="text" className="form-control mb-3 d-flex" id="CreationName" placeholder="Display name" />
                    <textarea ref="description" className="form-control mb-3 d-flex" id="CreationDescription" placeholder="Description" rows="3"></textarea>
                <small className="text-muted">This Service will be defaulted to offline</small>
                <br></br>
                <button onClick={this.onCreateService} className="btn btn-primary mr-auto d-flex px-4 py-2">Submit</button>
                <p>{this.state.message}</p>
                <div className="alert alert-success fade show d-none" id="service-create-succeed" role="alert">
                    <strong>Service creation succeeded</strong> the service has been created.

                    </div>
                <div className="alert alert-danger fade show d-none" role="alert" id="service-create-fail" role="alert">
                    error
                </div>
            </div>
        );
    }
}

export default CreateService
