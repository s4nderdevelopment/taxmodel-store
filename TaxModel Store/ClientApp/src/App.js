import React, { Component } from 'react';
import { Route } from 'react-router';
import { Layout } from './components/Layout';
import CreateBusinessPlan from "./components/create-business-plan/CreateBusinessPlan";
import EditBusinessPlan from "./components/edit-business-plan/EditBusinessPlan";
import CreateService from './components/create-service/CreateService';
import { withRouter } from 'react-router-dom';
import Home from './components/Home';
import Services from './components/store-front/Services'
import Contact from './components/store-front/Contact'
import BusinessPlan from './components/store-front/BusinessPlan'
import AdminOverview from "./components/admin-overview/AdminOverview";
import EditService from './components/edit-service/EditService';
import CreateAdmin from './components/create-account/CreateAdmin';
import CreateClient from './components/create-account/CreateClient';
import AdminLogin from './components/login/AdminLogin';
import ClientLogin from './components/login/ClientLogin';

import './custom.css'

class App extends Component {
    static displayName = App.name;

  render () {
    return (
      <Layout>
        <Route exact path='/' component={Home} />
        <Route path='/Services' component={Services} />
        <Route path='/Contact' component={Contact} />
        <Route path='/BusinessPlan' component={BusinessPlan} />
        <Route path='/CreateService' component={CreateService} />
        <Route path='/CreateBusinessPlan' component={CreateBusinessPlan} />
        <Route path='/AdminOverview' component={AdminOverview} />
        <Route path='/EditService/:name' component={EditService} />
        <Route path='/EditBusinessPlan/:name' component={EditBusinessPlan} />
        <Route path='/CreateClient' component={CreateClient} />
        <Route path='/CreateAdmin' component={CreateAdmin} />
        <Route path='/ClientLogin' component={ClientLogin} />
        <Route path='/AdminLogin' component={AdminLogin} />
      </Layout>
    );
  }
}

export default withRouter(App);
