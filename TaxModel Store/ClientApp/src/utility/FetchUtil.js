let config = {
    urlPrefix: "https://localhost:5001"
}

let FetchUtil = {
    fetchGet: function(url, options){
        options.method = "GET";
        if(!options["headers"]){
            options.headers = {};
        }
        options.headers["Content-Type"] = "application/json";
        options.headers["Authorization"] = "Bearer " + localStorage.getItem('token');
        return fetch(config.urlPrefix + url, options).then(function(response){
            if(!response.ok){
                throw new Error("Failed to fetch: " + response.status);
            }
            let jsonResponse = response.json();
            if(jsonResponse.error){
                throw new Error("Failed to fetch: " + jsonResponse.error);
            }
            return jsonResponse;
        });
    },
    fetchPost: function(url, options, postParameters){
        options.method = "POST";
        if(!options["headers"]){
            options.headers = {};
        }
        options.headers["Content-Type"] = "application/json";
        options.headers["Authorization"] = "Bearer " + localStorage.getItem('token');
        options.body = JSON.stringify(postParameters);
        return fetch(config.urlPrefix + url, options).then(function(response){
            if(!response.ok){
                throw new Error("Failed to fetch: " + response.status);
            }
            let jsonResponse = response.json();
            if(jsonResponse.error){
                throw new Error("Failed to fetch: " + jsonResponse.error);
            }
            return jsonResponse;
        });
    }
}

export default FetchUtil;
