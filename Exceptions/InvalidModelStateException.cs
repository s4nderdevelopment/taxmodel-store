﻿using System;

namespace TaxModel.Exceptions
{
    public class InvalidModelStateException : Exception
    {
        public InvalidModelStateException() : base("Invalid model state")
        {

        }
        public InvalidModelStateException(string description) : base("Invalid model state: " + description)
        {

        }
        public InvalidModelStateException(Exception exception) : base("Invalid model state", exception)
        {

        }
        public InvalidModelStateException(string description, Exception exception) : base("Invalid model state: " + description, exception)
        {

        }
    }
}
