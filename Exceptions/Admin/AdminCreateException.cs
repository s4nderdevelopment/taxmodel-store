﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaxModel.Exceptions.Admin
{
    public class AdminCreateException : Exception
    {
        public AdminCreateException() : base("Failed to create admin account") { }

        public AdminCreateException(string description) : base("Failed to create admin account:" + description) { }

        public AdminCreateException(Exception exception) : base("Failed to create admin account", exception) { }
        public AdminCreateException(string description, Exception exception) : base("Failed to create admin account: " + description, exception) { }
    }
}
