﻿using System;

namespace TaxModel.Exceptions.Admin
{
    public class AdminInvalidCredentialsException : Exception
    {
        public AdminInvalidCredentialsException() : base("Admin not found")
        {

        }
        public AdminInvalidCredentialsException(string description) : base("Admin not found: " + description)
        {

        }
        public AdminInvalidCredentialsException(Exception exception) : base("Admin not found", exception)
        {

        }
        public AdminInvalidCredentialsException(string description, Exception exception) : base("Admin not found: " + description, exception)
        {

        }

    }
}
