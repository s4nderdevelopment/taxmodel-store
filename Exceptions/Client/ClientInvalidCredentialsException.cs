﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TaxModel.Exceptions.Client
{
    public class ClientInvalidCredentialsException : Exception
    {
        public ClientInvalidCredentialsException() : base("Client not found")
        {

        }
        public ClientInvalidCredentialsException(string description) : base("Client not found: " + description)
        {

        }
        public ClientInvalidCredentialsException(Exception exception) : base("Client not found", exception)
        {

        }
        public ClientInvalidCredentialsException(string description, Exception exception) : base("Client not found: " + description, exception)
        {

        }
    }
}
