﻿using System;

namespace TaxModel.Exceptions.Client
{
    public class ClientCreateException : Exception
    {
        public ClientCreateException() : base("Failed to create client account") { }

        public ClientCreateException(string description) : base("Failed to create client account:" + description) { }

        public ClientCreateException(Exception exception) : base("Failed to create client account", exception) { }
        public ClientCreateException(string description, Exception exception) : base("Failed to create client account: " + description, exception) { }
    }
}
