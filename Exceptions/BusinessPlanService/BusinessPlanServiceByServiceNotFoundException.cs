﻿using System;

namespace TaxModel.Exceptions.BusinessPlanService
{
    public class BusinessPlanServiceByServiceNotFoundException : Exception
    {
        public BusinessPlanServiceByServiceNotFoundException(string serviceEntityName) : base("Business Plan Service not found by Service Name: " + serviceEntityName)
        {

        }
        public BusinessPlanServiceByServiceNotFoundException(string serviceEntityName, Exception exception) : base("Business Plan Service not found by Service Name: " + serviceEntityName, exception)
        {

        }
    }
}
