﻿using System;

namespace TaxModel.Exceptions.BusinessPlanService
{
    public class BusinessPlanServiceByBusinessPlanNotFoundException : Exception
    {
        public BusinessPlanServiceByBusinessPlanNotFoundException(string businessPlanEntityName) : base("Business Plan Service not found by Business Plan Name: " + businessPlanEntityName)
        {

        }
        public BusinessPlanServiceByBusinessPlanNotFoundException(string businessPlanEntityName, Exception exception) : base("Business Plan Service not found by Business Plan Name: " + businessPlanEntityName, exception)
        {

        }
    }
}
