﻿using System;

namespace TaxModel.Exceptions.Service
{
    public class ServiceNotFoundException : Exception
    {
        public ServiceNotFoundException() : base("Service not found")
        {

        }
        public ServiceNotFoundException(string description) : base("Service not found: " + description)
        {

        }
        public ServiceNotFoundException(Exception exception) : base("Service not found", exception)
        {

        }
        public ServiceNotFoundException(string description, Exception exception) : base("Service not found: " + description, exception)
        {

        }

    }
}
