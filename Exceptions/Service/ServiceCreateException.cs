﻿using System;

namespace TaxModel.Exceptions.Service
{
    public class ServiceCreateException : Exception
    {
        public ServiceCreateException() : base("Failed to create Service")
        {

        }
        public ServiceCreateException(string description) : base("Failed to create Service: " + description)
        {

        }
        public ServiceCreateException(Exception exception) : base("Failed to create Service", exception)
        {

        }
        public ServiceCreateException(string description, Exception exception) : base("Failed to create Service: " + description, exception)
        {

        }
    }
}
