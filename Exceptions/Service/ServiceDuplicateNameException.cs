﻿using System;

namespace TaxModel.Exceptions.Service
{
    public class ServiceDuplicateNameException : Exception
    {
        public ServiceDuplicateNameException() : base("Duplicate Service name")
        {

        }
        public ServiceDuplicateNameException(string description) : base("Duplicate Service name: " + description)
        {

        }
        public ServiceDuplicateNameException(Exception exception) : base("Duplicate Service name", exception)
        {

        }
        public ServiceDuplicateNameException(string description, Exception exception) : base("Duplicate Service name: " + description, exception)
        {

        }
    }
}
