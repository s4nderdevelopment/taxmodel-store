﻿using System;

namespace TaxModel.Exceptions.BusinessPlan
{
    public class BusinessPlanNotFoundException : Exception
    {
        public BusinessPlanNotFoundException() : base("Business Plan could not be found")
        {

        }
        public BusinessPlanNotFoundException(string descrition) : base("Business Plan could not be found: " + descrition)
        {

        }
        public BusinessPlanNotFoundException(Exception exception) : base("Business Plan could not be found", exception)
        {

        }
        public BusinessPlanNotFoundException(string descrition, Exception exception) : base("Business Plan could not be found: " + descrition, exception)
        {

        }
    }
}
