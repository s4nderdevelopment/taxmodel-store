﻿using System;

namespace TaxModel.Exceptions.BusinessPlan
{
    public class BusinessPlanDuplicateNameException : Exception
    {
        public BusinessPlanDuplicateNameException() : base("Duplicate Business Plan name")
        {

        }
        public BusinessPlanDuplicateNameException(string description) : base("Duplicate Business Plan name: " + description)
        {

        }

        public BusinessPlanDuplicateNameException(Exception exception) : base("Duplicate Business Plan name", exception)
        {

        }
        public BusinessPlanDuplicateNameException(string description, Exception exception) : base("Duplicate Business Plan name: " + description, exception)
        {

        }
    }
}
