﻿using System;

namespace TaxModel.Exceptions.BusinessPlan
{
    public class BusinessPlanPriceTooHighException : Exception
    {
        public BusinessPlanPriceTooHighException() : base("Business plan price is too high (above or equal to 10 000 000)")
        {

        }
        public BusinessPlanPriceTooHighException(string description) : base("Business plan price is too high (above or equal to 10 000 000): " + description)
        {

        }
        public BusinessPlanPriceTooHighException(Exception exception) : base("Business plan price is too high (above or equal to 10 000 000)", exception)
        {

        }
        public BusinessPlanPriceTooHighException(string description, Exception exception) : base("Business plan price is too high (above or equal to 10 000 000): " + description, exception)
        {

        }
    }
}
