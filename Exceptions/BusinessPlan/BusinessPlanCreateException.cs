﻿using System;

namespace TaxModel.Exceptions.BusinessPlan
{
    public class BusinessPlanCreateException : Exception
    {
        public BusinessPlanCreateException() : base("Failed to create Business Plan")
        {

        }

        public BusinessPlanCreateException(string description) : base("Failed to create Business Plan: " + description)
        {

        }

        public BusinessPlanCreateException(Exception exception) : base("Failed to create Business Plan", exception)
        {

        }

        public BusinessPlanCreateException(string description, Exception exception) : base("Failed to create Business Plan: " + description, exception)
        {

        }
    }
}
