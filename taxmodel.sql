-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 11, 2022 at 08:22 PM
-- Server version: 10.3.31-MariaDB-0+deb10u1
-- PHP Version: 7.3.31-1~deb10u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `taxmodel`
--

-- --------------------------------------------------------

--
-- Table structure for table `Admin`
--

CREATE TABLE `Admin` (
  `AdminID` binary(16) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Password` varchar(511) NOT NULL,
  `IsPrivileged` tinyint(4) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `Salt` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `BusinessPlan`
--

CREATE TABLE `BusinessPlan` (
  `BusinessPlanID` binary(16) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `DisplayName` varchar(255) NOT NULL,
  `Description` text NOT NULL,
  `Price` decimal(10,0) NOT NULL,
  `Status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `BusinessPlan`
--

INSERT INTO `BusinessPlan` (`BusinessPlanID`, `Name`, `DisplayName`, `Description`, `Price`, `Status`) VALUES
(0x052b463cc6d01946669108d9ed8fdaac, 'business-plan-1-458e56cc-90fd-458e-ad71-9d6c3a5320d0', 'Business plan 1', 'Description of Business plan 1: Some text here...', '432', 0);

-- --------------------------------------------------------

--
-- Table structure for table `BusinessPlanService`
--

CREATE TABLE `BusinessPlanService` (
  `BusinessPlanServiceID` binary(16) NOT NULL,
  `ServiceID` binary(16) NOT NULL,
  `BusinessPlanID` binary(16) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `BusinessPlanService`
--

INSERT INTO `BusinessPlanService` (`BusinessPlanServiceID`, `ServiceID`, `BusinessPlanID`) VALUES
(0x062b463cc6d01946669108d9ed8fdadc, 0xb67056575eda664a8e4808d9ed8ff813, 0x052b463cc6d01946669108d9ed8fdaac);

-- --------------------------------------------------------

--
-- Table structure for table `Client`
--

CREATE TABLE `Client` (
  `ClientID` binary(16) NOT NULL,
  `Password` varchar(511) NOT NULL,
  `Salt` varchar(255) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `PhoneNumber` varchar(255) NOT NULL,
  `CompanyName` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `Service`
--

CREATE TABLE `Service` (
  `ServiceID` binary(16) NOT NULL,
  `Name` varchar(255) NOT NULL,
  `DisplayName` varchar(255) NOT NULL,
  `Description` text NOT NULL,
  `Status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Service`
--

INSERT INTO `Service` (`ServiceID`, `Name`, `DisplayName`, `Description`, `Status`) VALUES
(0xb67056575eda664a8e4808d9ed8ff813, 'service-a-0829a5db-b050-41a3-b420-4998ee146119', 'Service A', 'Description of Service A', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Admin`
--
ALTER TABLE `Admin`
  ADD PRIMARY KEY (`AdminID`);

--
-- Indexes for table `BusinessPlan`
--
ALTER TABLE `BusinessPlan`
  ADD PRIMARY KEY (`BusinessPlanID`);

--
-- Indexes for table `BusinessPlanService`
--
ALTER TABLE `BusinessPlanService`
  ADD PRIMARY KEY (`BusinessPlanServiceID`);

--
-- Indexes for table `Service`
--
ALTER TABLE `Service`
  ADD PRIMARY KEY (`ServiceID`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
